# Karakuri OCaml
- This project is an OCaml conversion of https://gitlab.com/cogumbreiro/karukuri
- The purpose is to aim for faster run-time by writing the library in OCaml.
- More informations see _oasis file.

# Setup:
```bash
$ opam install cmdliner
$ opam install merlin
$ opam install oasis
$ opam install ocamlgraph
$ oasis setup -setup-update dynamic
$ make
```

# Example:
```bash
$ ./main.native --help
$ ./main.native examples/large.json --log --minimize
$ ./main.native examples/large.json --log --minimize --slow
$ ./main.native examples/small.json --minimize --output fsm
$ ./main.native examples/small.json --output fsm
$ ./main.native examples/ex1.json --state str --output string

$ ./main.native examples/dfa_exp1.json --input dfa_exp
$ ./main.native examples/dfa_exp1.json --i dfa_exp --slow
$ ./main.native examples/dfa_exp2.json -i dfa_exp -s str -c int

$ ./main.native examples/ex1.json --state str --dot
$ dot -Tpng out_ex1.dot -o ex1_graph.png
```
