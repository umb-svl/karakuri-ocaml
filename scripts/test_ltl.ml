open OUnit2
open Util
open Ltl

let assert_ltl_e (expected:ltl_e) (given:ltl_e) =
  let msg = "expected: " ^ ltl_e_to_string expected ^ " given: " ^ ltl_e_to_string given in
  assert_equal given expected ~msg

let tests = "test_ltl" >::: [
  "always" >:: (fun _ ->
      let p  = always (Var "x") |> ltl_to_e in
      let expected:ltl_e = WeakNext (always_e (Var "x")) in
      assert_ltl_e expected (big_step p ["x"]);
      assert_ltl_e (Or (Halt, always_e (Var "x"))) (advance expected |> list_or_e);
      ()
    )
  ]

let _ = run_test_tt_main tests