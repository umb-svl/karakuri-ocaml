open Exp;;

let test_count = ref 0;;
let pass_test () = test_count := !test_count + 1;;

(* ================================= tests ================================= *)
let a = Regular.DFA.(create
  (Hashset.of_list [0; 1]) 
  (transition_table [ 
    (("q_even", 0), "q_even");
    (("q_even", 1), "q_odd");
    (("q_odd", 0), "q_odd");
    (("q_odd", 1), "q_even") 
  ]) 
  "q_even" 
  (Accepted_List ["q_odd"]));;

let b = Regular.DFA.(create
  (Hashset.of_list [0; 1])
  (transition_table [
    (("q", 0), "q_0");
    (("q", 1), "q");
    (("q_0", 0), "q_00");
    (("q_0", 1), "q");
    (("q_00", 0), "q_00");
    (("q_00", 1), "q_001");
    (("q_001", 0), "q_001");
    (("q_001", 1), "q_001")
  ]) 
  "q"
  (Accepted_List ["q_001"]));;

let c = Regular.DFA.(create (* Accepts a series of 0s *)
  (Hashset.of_list [0; 1])
  (transition_table [
    (("q_1", 0), "q_1");
    (("q_1", 1), "q_2");
    (("q_2", 0), "q_2");
    (("q_2", 1), "q_2")
  ])
  "q_1"
  (Accepted_List ["q_1"]));;

let alp = Hashset.of_list [0; 1];;

let check_dfa bool_func exp_a exp_b =
  let a = DFAexp.eval alp exp_a in
  let b = DFAexp.eval alp exp_b in
  assert (bool_func a b);;

let check_nfa bool_func exp_a exp_b =
  let a = NFAexp.eval alp exp_a in
  let b = NFAexp.eval alp exp_b in
  assert (bool_func a b);;

let check_dfa_equivalent exp_a exp_b =
  check_dfa Regular.DFA.is_equivalent_to exp_a exp_b;;

let check_dfa_inequivalent exp_a exp_b =
  let bool_func a b = not (Regular.DFA.is_equivalent_to a b) in
  check_dfa bool_func exp_a exp_b;;

let check_dfa_equal exp_a exp_b =
  check_dfa Regular.DFA.equal exp_a exp_b;;

let check_dfa_unequal exp_a exp_b =
  let bool_func a b = not (Regular.DFA.equal a b) in
  check_dfa bool_func exp_a exp_b;;

let check_nfa_equal exp_a exp_b =
  check_nfa Regular.NFA.equal exp_a exp_b;;

let check_nfa_unequal exp_a exp_b =
  let bool_func a b = not (Regular.NFA.equal a b) in
  check_nfa bool_func exp_a exp_b;;

let test_dfa_not_eq () =
  check_dfa_inequivalent (D_Atom Nil) (D_Atom All);
  check_dfa_inequivalent (D_Atom Nil) (D_Atom Void);
  check_dfa_inequivalent (D_Atom All) (D_Atom Void);
  check_dfa_inequivalent (Dfa a) (Dfa b);
  check_dfa_inequivalent (Dfa a) (Dfa c);
  check_dfa_inequivalent (Dfa b) (Dfa c);
  check_dfa_inequivalent (Dfa a) (Complement (Dfa a));
  check_dfa_inequivalent (D_Union (Dfa a, Dfa b)) (D_Intersect (Dfa a, Dfa b));
  pass_test ();
  print_endline "test_dfa_not_eq";;

let test_dfa_union () =
  (* A ∪ (D_Atom Void) = A *)
  check_dfa_equivalent (D_Union (Dfa a, (D_Atom Void))) (Dfa a);
  (* A ∪ (D_Atom All) = (D_Atom All) *)
  check_dfa_equivalent (D_Union (Dfa a, (D_Atom All))) (D_Atom All);
  (* A ∪ A = A *)
  check_dfa_equivalent (D_Union (Dfa a, Dfa a)) (Dfa a);
  (* A ∪ A_complement = (D_Atom All) *)
  check_dfa_equivalent (D_Union (Dfa a, Complement (Dfa a))) (D_Atom All);
  (* A ∪ B = B ∪ A *)
  check_dfa_equivalent (D_Union (Dfa a, Dfa b)) (D_Union (Dfa b, Dfa a));
  (* (A ∪ B) ∪ C = A ∪ (B ∪ C) *)
  check_dfa_equivalent (D_Union (D_Union (Dfa a, Dfa b), Dfa c)) (D_Union (Dfa a, D_Union (Dfa b, Dfa c)));
  pass_test ();
  print_endline "test_dfa_union";;

let test_dfa_intersection () =
  (* A ∩ (D_Atom Void) = (D_Atom Void) *)
  check_dfa_equivalent (D_Intersect (Dfa a, (D_Atom Void))) (D_Atom Void);
  (* A ∩ (D_Atom All) = A *)
  check_dfa_equivalent (D_Intersect (Dfa a, (D_Atom All))) (Dfa a);
  (* A ∩ A = A *)
  check_dfa_equivalent (D_Intersect (Dfa a, Dfa a)) (Dfa a);
  (* A ∩ A_complement = (D_Atom Void) *)
  check_dfa_equivalent (D_Intersect (Dfa a, Complement (Dfa a))) (D_Atom Void);
  (* A ∩ B = B ∩ A *)
  check_dfa_equivalent (D_Intersect (Dfa a, Dfa b)) (D_Intersect (Dfa b, Dfa a));
  (* (A ∩ B) ∩ C = A ∩ (B ∩ C) *)
  check_dfa_equivalent 
    (D_Intersect (D_Intersect (Dfa a, Dfa b), Dfa c)) 
    (D_Intersect (Dfa a, D_Intersect (Dfa b, Dfa c)));
  pass_test ();
  print_endline "test_dfa_intersection";;

let test_dfa_subtract () =
  (* (D_Atom Void) - A = (D_Atom Void) *)
  check_dfa_equivalent (Subtract (D_Atom Void, Dfa a)) (D_Atom Void);
  (* (D_Atom All) - A = A_complement *)
  check_dfa_equivalent (Subtract (D_Atom All, Dfa a)) (Complement (Dfa a));
  (* A - (D_Atom Void) = A *)
  check_dfa_equivalent (Subtract (Dfa a, (D_Atom Void))) (Dfa a);
  (* A - (D_Atom All) = (D_Atom Void) *)
  check_dfa_equivalent (Subtract (Dfa a, (D_Atom All))) (D_Atom Void);
  (* A - A = (D_Atom Void) *)
  check_dfa_equivalent (Subtract (Dfa a, Dfa a)) (D_Atom Void);
  (* A - A_complement = A *)
  check_dfa_equivalent (Subtract (Dfa a, Complement (Dfa a))) (Dfa a);
  pass_test ();
  print_endline "test_dfa_subtract";;

let test_dfa_complement () =
  (* (D_Atom Void)_complement = (D_Atom All) *)
  check_dfa_equivalent (Complement (D_Atom Void)) (D_Atom All);
  (* (D_Atom All)_complement = (D_Atom Void) *)
  check_dfa_equivalent (Complement (D_Atom All)) (D_Atom Void);
  (* A_complement_complement = A *)
  check_dfa_equivalent (Complement (Complement (Dfa a))) (Dfa a);
  pass_test ();
  print_endline "test_dfa_complement";;

let test_dfa_distributtivity () =
  (* A ∩ (B ∪ C) = (A ∩ B) ∪ (A ∩ C) *)
  check_dfa_equivalent 
    (D_Intersect (Dfa a, (D_Union (Dfa b, Dfa c)))) 
    (D_Union (D_Intersect (Dfa a, Dfa b), D_Intersect (Dfa a, Dfa c)));
  (* A ∩ (B - C) = (A ∩ B) - (A ∩ C) *)
  check_dfa_equivalent 
    (D_Intersect (Dfa a, (Subtract (Dfa b, Dfa c)))) 
    (Subtract (D_Intersect (Dfa a, Dfa b), D_Intersect (Dfa a, Dfa c)));
  (* A ∪ (B ∩ C) = (A ∪ B) ∩ (A ∪ C) *)
  check_dfa_equivalent 
    (D_Union (Dfa a, (D_Intersect (Dfa b, Dfa c)))) 
    (D_Intersect (D_Union (Dfa a, Dfa b), D_Union (Dfa a, Dfa c)));
  pass_test ();
  print_endline "test_dfa_distributtivity";;

let test_dfa_optimize () =
  let open DFAexp in
  assert (equal (optimize (Complement (D_Atom Void))) (D_Atom All));
  assert (equal (optimize (Complement (Complement (Dfa a)))) (Dfa a));
  assert (equal (optimize (Subtract (D_Atom All, (D_Atom All)))) (D_Atom Void));
  pass_test ();
  print_endline "test_dfa_optimize";;

(*
let tmp () =
  (* for generating an example *)
  let open Yojson.Basic.Util in
  let f = "examples/large.json" in
  let json = Yojson.Basic.from_file f in
  let a = Parser.DFA.of_json json to_int filter_int to_string in
  let exp = Minimize (
    D_Union (
      Dfa a, 
      D_Union (
        D_Union (Dfa a, Minimize (Dfa a)), 
        D_Union (
          Dfa a, 
          D_Union (
            Dfa a, 
            D_Union (
              Dfa a, 
              D_Union (
                D_Union (D_Union (Dfa a, Dfa a), Dfa a), 
                D_Union (Minimize (Minimize (Dfa a)), Dfa a)))))))) in
  let exp_void = D_Intersect (exp, Complement (Minimize (Dfa a))) in
  let str = Yojson.Basic.pretty_to_string (Parser.exp_to_json exp_void (fun st -> `Int st) (fun char -> `String char)) in
  print_endline str;;
*)
(* =============================== run tests =============================== *)
let main () =
  test_dfa_not_eq ();
  test_dfa_union ();
  test_dfa_intersection ();
  test_dfa_subtract ();
  test_dfa_complement ();
  test_dfa_distributtivity ();
  test_dfa_optimize ();;

let () = 
  Printexc.record_backtrace true;
  main ();
  Printf.printf "Passed tests: %d\n" !test_count;;