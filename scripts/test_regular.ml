open Util;;
open Regular;;

let test_count = ref 0;;
let pass_test () = test_count := !test_count + 1;;

(* ================================= tests ================================= *)
let test_fig1_2 () =
  (* Fig 1.2 of the ITC Sipser book *)
  let open DFA in
  let input = Hashset.of_list ["neither"; "front"; "rear"; "both"] in
  let state_transition old_st i =
    if old_st = "close" && i = "front" then "open" else
    if old_st = "open" && i = "neither" then "close" else old_st in
  let a = create input state_transition "close" (Accepted_List ["close"]) in
  assert (accepts a ["front"; "neither"]);
  assert (not (accepts a ["front"]));
  assert (not (accepts a ["front"; "front"]));
  assert ((get_shortest_string a) = Some []);
  pass_test ();
  print_endline "pass test_fig1_2";;
  
let test_shortest_string () =
  let open DFA in
  let a = create
    (Hashset.of_list [0; 1]) 
    (transition_table [ 
      (("q_even", 0), "q_even");
      (("q_even", 1), "q_odd");
      (("q_odd", 0), "q_odd");
      (("q_odd", 1), "q_even") 
    ]) 
    "q_even" 
    (Accepted_List ["q_odd"]) in
  assert ((get_shortest_string a) = Some [1]);
  let a = create
    (Hashset.of_list [0; 1])
    (transition_table [
      (("q", 0), "q_0");
      (("q", 1), "q");
      (("q_0", 0), "q_00");
      (("q_0", 1), "q");
      (("q_00", 0), "q_00");
      (("q_00", 1), "q_001");
      (("q_001", 0), "q_001");
      (("q_001", 1), "q_001")
    ]) 
    "q"
    (Accepted_List ["q_001"]) in
  assert ((get_shortest_string a) = Some [0; 0; 1]);
  pass_test ();
  print_endline "pass test_shortest_string";;

let test_divergence_index () =
  (* Accepts a series of 0s *)
  let open DFA in
  let a = create
    (Hashset.of_list [0; 1])
    (transition_table [
      (("q_1", 0), "q_1");
      (("q_1", 1), "q_2");
      (("q_2", 0), "q_2");
      (("q_2", 1), "q_2")
    ])
    "q_1"
    (Accepted_List ["q_1"]) in
  assert ((get_divergence_index a []) = None);
  assert ((get_divergence_index a [1]) = Some 0);
  assert ((get_divergence_index a [0; 1]) = Some 1);
  assert ((get_divergence_index a [0; 0; 1]) = Some 2);
  assert ((get_divergence_index a [0; 0; 0; 1]) = Some 3);
  assert ((get_divergence_index a [0; 0; 0; 1; 1]) = Some 3);
  pass_test ();
  print_endline "pass test_divergence_index";;

let test_fig1_4 () =
  let open DFA in
  let state_transition src i =
    if src = 1 && i = 0 then 1 else
    if src = 1 && i = 1 then 2 else
    if src = 2 && i = 0 then 3 else
    if src = 2 && i = 1 then 2 else
    if src = 3 then 2 else assert false in
  let a = create (Hashset.of_list [0; 1]) state_transition 1 (Accepted_List [2]) in
  List.iter (fun word -> assert (accepts a word))
    [[1]; [0;1]; [1;1]; [0;1;0;1;0;1;0;1;0;1]; [1;0;0]; [0;1;0;0]];
  List.iter (fun word -> assert (not (accepts a word)))
    [[0]; [1;0]; [1;0;1;0;0;0]];
  (* Alternative definition of a DFA, by giving the transition table *)
  let a = create
    (Hashset.of_list [0; 1])
    (transition_table [
      ((1,0), 1);
      ((1,1), 2);
      ((2,0), 3);
      ((2,1), 2);
      ((3,0), 2);
      ((3,1), 2)
    ])
    1
    (Accepted_List [2]) in
  List.iter (fun word -> assert (accepts a word))
    [[1]; [0;1]; [1;1]; [0;1;0;1;0;1;0;1;0;1]; [1;0;0]; [0;1;0;0]];
  List.iter (fun word -> assert (not (accepts a word)))
    [[0]; [1;0]; [1;0;1;0;0;0]];
  pass_test ();
  print_endline "pass test_fig1_4";;

let b_p = ref "b.pressed";;
let b_r = ref "b.release";;

let create_button () = 
  let open NFA in
  create
    (Hashset.of_list [!b_p; !b_r])
    (transition_edges [
      (0, [Some !b_p], 1);
      (1, [Some !b_r], 0)
    ])
    0
    (Accepted_List [0; 1]);;

let la_on = ref "la.on";;
let la_off = ref "la.off";;

let create_led_a () =
  let open NFA in
  create
    (Hashset.of_list [!la_on; !la_off])
    (transition_edges [
      (0, [Some !la_on], 1);
      (1, [Some !la_off], 0)
    ])
    0
    (Accepted_List [0; 1]);;

let lb_on = ref "lb.on";;
let lb_off = ref "lb.off";;

let create_led_b () =
  let open NFA in
  create
    (Hashset.of_list [!lb_on; !lb_off])
    (transition_edges [
      (0, [Some !lb_on], 1);
      (1, [Some !lb_off], 0)
    ])
    0
    (Accepted_List [0; 1]);;

let t_t = ref "t.t";;
let t_c = ref "t.c";;
let t_s = ref "t.s";;

let create_timer () =
  let open NFA in
  create
    (Hashset.of_list [!t_t; !t_c; !t_s])
    (transition_edges [
      (0, [Some !t_s], 1);
      (1, [(Some !t_c); (Some !t_t)], 0)
    ])
    0
    (Accepted_List [0; 1]);;

let level1 = ref "l1";;
let level2 = ref "l2";;
let standby1 = ref "s1";;
let standby2 = ref "s2";;

let create_hello_world () =
  let open NFA in
  create
    (Hashset.of_list [!level1; !level2; !standby1; !standby2])
    (transition_edges_split [
      (0, Some !level1, 1);
      (1, Some !level2, 2);
      (1, Some !standby1, 0);
      (2, Some !standby2, 0)
    ])
    0
    (Accepted_Function (fun x -> 0 <= x && x <= 2));;

let create_led_and_button () =
  (*  This example should be a sub-behavior of shuffling button with led-a. 
      (0) LEDA.ON --->  (1)  BTN.PRS ----> (2)
          <--- LEDA.OFF      <--- BTN.REL *)
  let open NFA in
  create
    (Hashset.of_list [!la_on; !la_off; !b_p; !b_r])
    (transition_edges [
      (0, [Some !la_on], 1);
      (1, [Some !la_off], 0);
      (1, [Some !b_p], 2);
      (2, [Some !b_r], 1)
    ])
    0
    (Accepted_List [0; 1; 2]);;

let test_button () =
  let open NFA in
  let button = create_button () in
  assert (accepts button []);
  assert (accepts button [!b_p; !b_r]);
  assert (accepts button [!b_p]);
  assert (not (accepts button [!b_r]));
  pass_test ();
  print_endline "pass test_button";;

let test_convert_dfa () =
  let open DFA in
  let button = create_button () in
  let button = nfa_to_dfa button in
  assert (accepts button []);
  assert (accepts button [!b_p; !b_r]);
  assert (accepts button [!b_p]);
  assert (not (accepts button [!b_r]));
  pass_test ();
  print_endline "pass test_convert_dfa";;

let test_complement () =
  (* A simple test on the complement of a DFA *)
  let open DFA in
  let button = create_button () in
  let button = nfa_to_dfa button in
  let button = complement button in
  assert (not (accepts button []));
  assert (not (accepts button [!b_p; !b_r]));
  assert (not (accepts button [!b_p]));
  assert (accepts button [!b_r]);
  pass_test ();
  print_endline "pass test_complement";;

let test_shuffle () =
  let open NFA in
  let button = create_button () in
  let led_a = create_led_a () in
  let both = shuffle button led_a in
  (* Both should accept all behaviors of the button *)
  assert (accepts both []);
  assert (accepts both [!b_p; !b_r]);
  assert (accepts both [!b_p]);
  assert (not (accepts both [!b_r]));
  (* It should also accept all behaviors of led *)
  assert (accepts both [!la_on; !la_off]);
  assert (accepts both [!la_on]);
  assert (not (accepts both [!la_off]));
  (* Finally, it should accept interleaving of LED and Button: *)
  assert (accepts both [!la_on; !b_p; !b_r; !la_off; !b_p]);
  (* Here we fail because we have B_P followed by B_P *)
  assert (not (accepts both [!la_on; !b_p; !la_off; !b_p]));
  pass_test ();
  print_endline "pass test_shuffle";;

let test_shuffle_2 () =
  let open NFA in
  let n1 = create
    (Hashset.of_list ['a'; 'b'])
    (transition_edges_split [
      (0, Some 'a', 1);
      (1, Some 'b', 2);
      (2, Some 'a', 1)
    ])
    0
    (Accepted_List [1; 2]) in
  let n2 = create
    (Hashset.of_list ['c'; 'd'])
    (transition_edges_split [
      (0, Some 'c', 1);
      (1, Some 'd', 2);
      (2, Some 'c', 1)
    ])
    0
    (Accepted_List [1; 2]) in
  let both = shuffle n1 n2 in
  print_endline (to_string both (fun (s1, s2) -> Printf.sprintf "(%d, %d)" s1 s2) Char.escaped);
  (* Both should accept all behaviors of the button *)
  assert (not (accepts both []));
  assert (accepts both ['a']);
  assert (accepts both ['c']);
  assert (accepts both ['a'; 'c']);
  assert (accepts both ['c'; 'a']);
  assert (not (accepts both ['d'; 'c'; 'a']));
  pass_test ();
  print_endline "pass test_shuffle_2";;

let test_sink () =
  (* Non-trivial sink state that is involved in a cycle of sink-states. *)
  let open NFA in
  let n1 = create
    (Hashset.of_list ['a'; 'b'])
    (transition_edges_split [
      (0, Some 'a', 1);
      (1, Some 'b', 2);
      (2, Some 'a', 3);
      (3, Some 'a', 4);
      (4, Some 'a', 3);
      (0, Some 'a', 5);
      (5, Some 'a', 5)
    ])
    0
    (Accepted_List [1; 2]) in
  assert (is_sink n1 5);
  assert (is_sink n1 3);
  assert (is_sink n1 4);
  pass_test ();
  print_endline "pass test_sink";;

let test_led_and_button () =
  let open NFA in
  let both = create_led_and_button () in
  (* Both should accept all behaviors of the button *)
  assert (accepts both []);
  assert (accepts both [!la_on]);
  assert (accepts both [!la_on; !la_off]);
  (* It should also accept all behaviors of led *)
  assert (accepts both [!la_on; !b_p]);
  assert (accepts both [!la_on; !b_p; !b_r; !la_off]);
  assert (accepts both [!la_on; !b_p; !b_r; !la_off; !la_on]);
  (* Here we fail because we have B_P followed by B_P *)
  assert (not (accepts both [!b_r]));
  assert (not (accepts both [!b_p]));
  assert (not (accepts both [!la_on; !b_r]));
  assert (not (accepts both [!la_on; !b_p; !la_off]));
  pass_test ();
  print_endline "pass test_led_and_button";;

let test_contains () =
  let button = create_button () in
  let led_a = create_led_a () in
  (* All behaviors of button and leda *)
  let both = NFA.shuffle button led_a in
  let large = nfa_to_dfa both in
  (* We describe a behavior that is smaller than the shuffling of all *)
  let behavior = create_led_and_button () in
  let small = nfa_to_dfa behavior in
  assert (DFA.contains large small);
  pass_test ();
  print_endline "pass test_contains";;

let test_hello_world_not_empty () =
  let hw = create_hello_world () in
  let hw_d = nfa_to_dfa hw in
  let hw_s = DFA.minimize hw_d in
  let hw_r = nfa_to_regex hw in
  assert (not (NFA.is_empty hw));
  assert (not (DFA.is_empty hw_d));
  assert (not (DFA.is_empty hw_s));
  (* We know that it should not be NIL, it should accept more *)
  assert (hw_r <> Nil);
  assert (hw_r <> Void);
  pass_test ();
  print_endline "pass test_hello_world_not_empty";;

let assert_gnfa before after =
  let open REGEX in
  let states = ref [] in
  let alphabet = ref [] in
  let tsx = Hashtbl.create !init_size in
  let before_iter_func ((src, dst), char) =
    states := src :: dst :: !states;
    alphabet := char :: !alphabet;
    Hashtbl.replace tsx (src, dst) (Char char) in
  List.iter before_iter_func before;
  let gn = GNFA.create
    (Hashset.of_list !states)
    (Hashset.of_list !alphabet)
    tsx
    0
    2 in
  let next_gn = snd (GNFA.step ~node:1 gn) in
  let after_tbl = Hashtbl.create (List.length after) in
  List.iter (fun (k, v) -> Hashtbl.replace after_tbl k v) after;
  assert (hashtbl_equal next_gn.transitions after_tbl);;

let test_gnfa_step1 () =
  let open REGEX in
  let before = [
    ((0, 1), "a");
    ((1, 2), "b")
  ] in
  let after = [
    ((0, 2), Concat (Char "a", Char "b"))
  ] in
  assert_gnfa before after;
  pass_test ();
  print_endline "pass test_gnfa_step1";;

let test_gnfa_step2 () =
  let open REGEX in
  let before = [
    ((0, 1), "a");
    ((1, 2), "b");
    ((0, 3), "c");
    ((3, 1), "d")
  ] in
  let after = [
    ((0, 2), Concat (Char "a", Char "b"));
    ((0, 3), Char "c");
    ((3, 2), Concat (Char "d", Char "b"))
  ] in
  assert_gnfa before after;
  pass_test ();
  print_endline "pass test_gnfa_step2";;

let test_gnfa_step3 () =
  let open REGEX in
  let before = [
    ((1, 2), "b");
    ((0, 3), "c");
    ((3, 1), "d");
    ((3, 2), "e")
  ] in
  let after = [
    ((0, 3), Char "c");
    ((3, 2), Union (Concat (Char "d", Char "b"), Char "e"))
  ] in
  assert_gnfa before after;
  pass_test ();
  print_endline "pass test_gnfa_step3";;

let test_gnfa_step4 () =
  let open REGEX in
  (* Self-loop *)
  let before = [
    ((0, 1), "a");
    ((1, 2), "b");
    ((1, 1), "c")
  ] in
  let after = [
    ((0, 2), Concat (Char "a", Concat (Star (Char "c"), Char "b")))
  ] in
  assert_gnfa before after;
  pass_test ();
  print_endline "pass test_gnfa_step4";;

let test_gnfa_step5 () =
  let open REGEX in
  let before = [
    ((0, 1), "a");
    ((1, 2), "b");
    ((1, 0), "c")
  ] in
  let after = [
    ((0, 0), Concat (Char "a", Char "c"));
    ((0, 2), Concat (Char "a", Char "b"))
  ] in
  assert_gnfa before after;
  pass_test ();
  print_endline "pass test_gnfa_step5";;

let test_nfa_concat () =
  let open NFA in
  let alpha = Hashset.of_list ["a"; "b"] in
  let a1 = create
    alpha
    (transition_table [
      ((1, Some "a"), [1; 2; 3]);
      ((1, None),     [3]);
      ((2, Some "b"), [2]);
      ((2, Some "a"), [1; 3]);
      ((3, None),     [1]);
      ((3, Some "b"), [4])
    ])
    1
    (Accepted_List [2; 4]) in
  let a2 = create
    alpha
    (transition_table [
      ((0, None),     [1]);
      ((1, Some "a"), [2]);
      ((2, None),     [3]);
      ((3, Some "b"), [4]);
      ((4, None),     [0])
    ])
    0
    (Accepted_List [0]) in
  let expected = create
    alpha
    (transition_table [
      ((1, Some "a"), [1; 2; 3]);
      ((1, None),     [3]);
      ((2, Some "b"), [2]);
      ((2, Some "a"), [1; 3]);
      ((2, None),     [5]);
      ((3, None),     [1]);
      ((3, Some "b"), [4]);
      ((4, None),     [5]);
      ((5+0, None),     [5+1]);
      ((5+1, Some "a"), [5+2]);
      ((5+2, None),     [5+3]);
      ((5+3, Some "b"), [5+4]);
      ((5+4, None),     [5+0]);
    ])
    1
    (Accepted_List [5+0]) in
  let a1_a2 = concat a1 a2 in
  Printf.printf "EXPECTED:\n%s\n" (to_string (flatten expected) string_of_int (fun str -> str));
  Printf.printf "GIVEN:\n%s\n" (to_string (flatten a1_a2) string_of_int (fun str -> str));
  assert (equal a1_a2 expected);
  pass_test ();
  print_endline "pass test_nfa_concat";;

let test_eq_1 () =
  (* Show that the order of the alphabet has no impact in equality *)
  let open NFA in
  let a1 = create
    (Hashset.of_list ["a"; "b"])
    (transition_table [
      ((0, None),     [1]);
      ((1, Some "a"), [2]);
      ((2, None),     [3]);
      ((3, Some "b"), [4]);
      ((4, None),     [0])
    ])
    0
    (Accepted_List [0]) in
  let a2 = create
    (Hashset.of_list ["a"; "b"])
    (transition_table [
      ((0, None),     [1]);
      ((1, Some "a"), [2]);
      ((2, None),     [3]);
      ((3, Some "b"), [4]);
      ((4, None),     [0])
    ])
    0
    (Accepted_List [0]) in
  assert (equal a1 a2);
  pass_test ();
  print_endline "pass test_eq_1";;

let test_nfa_star () =
  let open NFA in
  let alpha = Hashset.of_list ["a"; "b"] in
  let a1 = create
    alpha
    (transition_table [
      ((1, Some "a"), [2]);
      ((2, None),     [3]);
      ((3, Some "b"), [4])
    ])
    1
    (Accepted_List [4]) in
  let expected = create
    alpha
    (transition_table [
      ((0, None),     [1]);
      ((1, Some "a"), [2]);
      ((2, None),     [3]);
      ((3, Some "b"), [4]);
      ((4, None),     [0])
    ])
    0
    (Accepted_List [0]) in
  assert (equal (star a1) expected);
  pass_test ();
  print_endline "pass test_nfa_star";;

let test_step_algo () = 
  let open REGEX in
  let r1 = Char "d" in
  let r2 = Void in
  let r3 = Char "b" in
  let r4 = Char "e" in
  assert ((union (concat r1 (concat (star r2) r3)) r4) =
    (Union ((Concat (r1, r3)), r4)));
  let r1 = Char "a" in
  let r2 = Char "b" in
  let r3 = Char "c" in
  let r4 = Char "d" in
  assert ((union (concat r1 (concat (star r2) r3)) r4) =
    (Union ((Concat (r1, (Concat ((Star r2), r3)))), r4)));
  let r1 = Void in
  let r2 = Char "b" in
  let r3 = Char "c" in
  let r4 = Char "d" in
  assert ((union (concat r1 (concat (star r2) r3)) r4) = r4);
  let r1 = Void in
  let r2 = Char "b" in
  let r3 = Void in
  let r4 = Char "d" in
  assert ((union (concat r1 (concat (star r2) r3)) r4) = r4);
  let r1 = Char "a" in
  let r2 = Void in
  let r3 = Char "b" in
  let r4 = Void in
  assert ((union (concat r1 (concat (star r2) r3)) r4) =
    (Concat (Char "a", Char "b")));
  pass_test ();
  print_endline "pass test_step_algo";;

let test_transition_edges_split () =
  let open NFA in
  let tsx = transition_edges_split [
    (0, Some !level1, 1);
    (1, Some !level2, 2);
    (1, Some !standby1, 0);
    (2, Some !standby2, 0);
    (1, Some !standby1, 1)
  ] in
  let tsx_func src ch_opt =
    let src_ch = (src, ch_opt) in
    if src_ch = (0, Some !level1) then [1] else
    if src_ch = (1, Some !level2) then [2] else
    if src_ch = (1, Some !standby1) then [0; 1] else
    if src_ch = (2, Some !standby2) then [0] else [] in
  for src = 0 to 2 do
    List.iter 
      (fun k -> assert ((Hashset.of_list (tsx src (Some k))) = (Hashset.of_list (tsx_func src (Some k))))) 
      [!level1; !level2; !standby1; !standby2]
  done;
  pass_test ();
  print_endline "pass test_transition_edges_split";;

let test_dfa_edges_1 () =
  let n = create_hello_world () in
  let d = nfa_to_dfa n in
  assert (Hashset.equal (Hashset.of_stream (DFA.edges d)) (Hashset.of_list [
    ([0], !level1, [1]);
    ([0], !level2, []);
    ([0], !standby1, []);
    ([0], !standby2, []);
    ([], !level1, []);
    ([], !level2, []);
    ([], !standby1, []);
    ([], !standby2, []);
    ([1], !level1, []);
    ([1], !level2, [2]);
    ([1], !standby1, [0]);
    ([1], !standby2, []);
    ([2], !level1, []);
    ([2], !level2, []);
    ([2], !standby1, []);
    ([2], !standby2, [0])
  ]));
  pass_test ();
  print_endline "pass test_dfa_edges_1";;

let sort_outgoings e =
  let edges_map_func (src, outs) =
    let sort_outs (c1, d1) (c2, d2) =
      if c1 = c2 
      then begin
        if d1 = d2 
        then 0
        else if d1 > d2 then 1 else -1
      end else if c1 > c2 then 1 else -1 in
  (src, List.sort sort_outs outs) in
  List.sort 
    (fun (s1, _) (s2, _) -> if s1 = s2 then 0 else if s1 > s2 then 1 else -1)
    (List.map edges_map_func e);;

let test_dfa_iter_outgoing_1 () =
  let n = create_hello_world () in
  let d = nfa_to_dfa n in
  let d_outgoings = rev_list_of_stream (DFA.iter_outgoing d) in
  let expected = [
    ([0], [
      (!level1, [1]);
      (!level2, []);
      (!standby1, []);
      (!standby2, [])
    ]);
    ([], [
      (!level1, []);
      (!level2, []);
      (!standby1, []);
      (!standby2, [])
    ]);
    ([1], [
      (!level1, []);
      (!level2, [2]);
      (!standby1, [0]);
      (!standby2, [])
    ]);
    ([2], [
      (!level1, []);
      (!level2, []);
      (!standby1, []);
      (!standby2, [0])
    ]);
  ] in
  (* sorting for comparison *)
  assert ((sort_outgoings d_outgoings) = (sort_outgoings expected));
  pass_test ();
  print_endline "pass test_dfa_iter_outgoing_1";;

let test_nfa_edges_1 () =
  let n = create_hello_world () in
  assert (Hashset.equal (Hashset.of_stream (NFA.edges n)) (Hashset.of_list [
    (0, Some !level1, [1]);
    (0, Some !level2, []);
    (0, Some !standby1, []);
    (0, Some !standby2, []);
    (0, None, []);
    (1, Some !level1, []);
    (1, Some !level2, [2]);
    (1, Some !standby1, [0]);
    (1, Some !standby2, []);
    (1, None, []);
    (2, Some !level1, []);
    (2, Some !level2, []);
    (2, Some !standby1, []);
    (2, Some !standby2, [0]);
    (2, None, []);
  ]));
  pass_test ();
  print_endline "pass test_nfa_edges_1";;

let test_nfa_iter_outgoing_1 () =
  let n = create_hello_world () in
  let n_outgoings = rev_list_of_stream (NFA.iter_outgoing n) in
  let expected = [
    (0, [
      (Some !level1, [1]);
      (Some !level2, []);
      (Some !standby1, []);
      (Some !standby2, []);
      (None, [])
    ]);
    (1, [
      (Some !level1, []);
      (Some !level2, [2]);
      (Some !standby1, [0]);
      (Some !standby2, []);
      (None, [])
    ]);
    (2, [
      (Some !level1, []);
      (Some !level2, []);
      (Some !standby1, []);
      (Some !standby2, [0]);
      (None, [])
    ]);
  ] in
  (* sorting for comparison *)
  assert ((sort_outgoings n_outgoings) = (sort_outgoings expected));
  pass_test ();
  print_endline "pass test_nfa_iter_outgoing_1";;

let test_nfa_state_diagram_1 () =
  let n = create_hello_world () in
  let (vs, es) = NFA.as_graph n in
  assert ((List.sort Int.compare (Hashset.to_list vs)) = [0; 1; 2]);
  let open Hashtbl in
  let tbl = create 4 in
  add tbl (0, 1) (Hashset.of_list [Some !level1]);
  add tbl (1, 2) (Hashset.of_list [Some !level2]);
  add tbl (1, 0) (Hashset.of_list [Some !standby1]);
  add tbl (2, 0) (Hashset.of_list [Some !standby2]);
  assert (hashtbl_equal ~val_equal:Hashset.equal es tbl);
  pass_test ();
  print_endline "pass test_nfa_state_diagram_1";;

let test_minimize () =
  (*  (0) --> LA_ON ---> (1)
      (1) --> LA_OFF --> (2)
      (2) --> LA_ON --> (3)
      (3) --> LA_OFF --> (0) *)
  let open DFA in
  let example = create
    (Hashset.of_list [!la_on; !la_off])
    (transition_table ~sink:99 [
      ((0, !la_on), 1);
      ((1, !la_off), 2);
      ((2, !la_on), 3);
      ((3, !la_off), 0)
    ])
    0
    (Accepted_List [0; 1; 2; 3]) in
  (* The above DFA has 5 states *)
  assert ((size example) = 5);
  (* Minimizing should yield only 3 states *)
  assert ((size (minimize example)) = 3);
  pass_test ();
  print_endline "pass test_minimize";;

let test_subtract () =
  (* Sigma* *)
  let open DFA in
  let l = create
    (Hashset.of_list [0; 1])
    (transition_table [
      (("s", 0), "s");
      (("s", 1), "s")
    ])
    "s"
    (Accepted_List ["s"]) in
  (* Empty string *)
  let r = make_nil (Hashset.of_list [0; 1]) in
  let result = subtract l r in
  let expected = create
    (Hashset.of_list [0; 1])
    (transition_table [
      (("s", 0), "q");
      (("s", 1), "q");
      (("q", 1), "q");
      (("q", 0), "q")
    ])
    "s"
    (Accepted_List ["q"]) in
  assert (is_equivalent_to result expected);
  pass_test ();
  print_endline "pass test_subtract";;

let test_set_alphabet () =
  (* Accepts a* *)
  let open DFA in
  let a = create
    (Hashset.of_list ["a"])
    (transition_table [((0, "a"), 0)])
    0
    (Accepted_List [0]) in
  (* Extend alphabet to accept a and b. *)
  let b = set_alphabet a (Hashset.of_list ["a"; "b"]) in
  assert (accepts b ["a"; "a"]);
  assert (not (accepts b ["a"; "b"]));
  (* Set alphabet to not contain "a" *)
  let c = set_alphabet a (Hashset.of_list ["b"] )in
  assert (is_equivalent_to c (make_nil (Hashset.of_list ["b"])));
  pass_test ();
  print_endline "pass test_set_alphabet";;

let test_dfa_as_dict () =
  let open DFA in
  let a = create
    (Hashset.of_list ['a'; 'b'])
    (transition_table [
      ((1,'a'), 1);
      ((1,'b'), 2);
      ((2,'a'), 3);
      ((2,'b'), 2);
      ((3,'a'), 2);
      ((3,'b'), 2)
    ])
    1
    (Accepted_List [2]) in
  let d = as_dict ~is_flatten:false a in
  assert (d.start_state = 1);
  assert (d.accepted_states = [2]);
  let edges_compare_func a b =
    let a_src = a.src and b_src = b.src in
    let a_char = a.char and b_char = b.char in
    let a_dst = a.dst and b_dst = b.dst in
    if a_src = b_src
    then begin
      if a_char = b_char
      then begin
        if a_dst = b_dst then 0
        else (if a_dst > b_dst then 1 else -1)
      end else (if a_char > b_char then 1 else -1)
    end else (if a_src > b_src then 1 else -1) in
  assert ((List.sort edges_compare_func d.edges) = [
    { src = 1; char = 'a'; dst = 1 }; 
    { src = 1; char = 'b'; dst = 2 }; 
    { src = 2; char = 'a'; dst = 3 };
    { src = 2; char = 'b'; dst = 2 };
    { src = 3; char = 'a'; dst = 2 }; 
    { src = 3; char = 'b'; dst = 2 }
  ]);
  pass_test ();
  print_endline "pass test_dfa_as_dict";;

let test_dfa_get_derivation () =
  let open DFA in
  let a = create
    (Hashset.of_list ['a'; 'b'])
    (transition_table [
      ((1,'a'), 1);
      ((1,'b'), 2);
      ((2,'a'), 3);
      ((2,'b'), 2);
      ((3,'a'), 2);
      ((3,'b'), 2)
    ])
    1
    (Accepted_List [2]) in
  assert ((get_derivation a ['a'; 'b'; 'a']) = [1; 2; 3]);
  assert ((get_derivation a ['a'; 'b']) = [1; 2]);
  assert ((get_derivation a ['a'; 'b'; 'a'; 'a']) = [1; 2; 3; 2]);
  pass_test ();
  print_endline "pass test_dfa_get_derivation";;

let test_nfa_get_derivation () =
  (* Test a deterministic NFA: *)
  let open NFA in
  let a = create
    (Hashset.of_list ['a'; 'b'])
    (transition_edges_split [
      (1, Some 'a', 1);
      (1, Some 'b', 2);
      (2, Some 'a', 3);
      (2, Some 'b', 2);
      (3, Some 'a', 2);
      (3, Some 'b', 2)
    ])
    1
    (Accepted_List [2]) in
  assert ((get_derivations a [Some 'a'; Some 'b'; Some 'a']) = [[1;2;3]]);
  assert ((get_derivations a [Some 'a'; Some 'b']) = [[1;2]]);
  assert ((get_derivations a [Some 'a'; Some 'b'; Some 'a'; Some 'a']) = [[1;2;3;2]]);
  (* Test a self loop *)
  let a = create
    (Hashset.of_list ['a'; 'b'])
    (transition_edges_split [
      (1, None, 1)
    ])
    1
    (Accepted_List [1]) in
  assert ((get_derivations a []) = [[]]);
  assert ((get_derivations a [Some 'a']) = []);
  (* a* b+ a+ *)
  let a = create
    (Hashset.of_list ['a'; 'b'])
    (transition_edges_split [
      (1, Some 'a', 1);
      (1, Some 'b', 2);
      (2, Some 'b', 2);
      (2, Some 'a', 3);
      (3, Some 'a', 3);
      (1, None, 4);
      (4, Some 'a', 5);
      (5, Some 'b', 6);
      (6, Some 'a', 7);
      (7, None, 3)
    ])
    1
    (Accepted_List [3; 7]) in
  assert ((List.sort compare (get_derivations a [Some 'a'; Some 'b'; Some 'a'])) = [
    [1;2;3]; 
    [4;5;6;7]
  ]);
  pass_test ();
  print_endline "pass test_nfa_get_derivation";;

let test_nfa_as_dict () =
  let open NFA in
  let a = create
    (Hashset.of_list ['a'; 'b'])
    (transition_table [
      ((1, Some 'a'), [1]);
      ((1, Some 'b'), [2]);
      ((2, Some 'a'), [3]);
      ((2, Some 'b'), [2]);
      ((3, Some 'a'), [2]);
      ((3, Some 'b'), [2]);
    ])
    1
    (Accepted_List [2]) in
  let d = as_dict ~is_flatten:false a in
  assert (d.start_state = 1);
  assert (d.accepted_states = [2]);
  let edges_compare_func a b =
    let a_src = a.src and b_src = b.src in
    let a_char = a.char and b_char = b.char in
    let a_dst = a.dst and b_dst = b.dst in
    if a_src = b_src
    then begin
      if a_char = b_char
      then begin
        if a_dst = b_dst then 0
        else (if a_dst > b_dst then 1 else -1)
      end else (if a_char > b_char then 1 else -1)
    end else (if a_src > b_src then 1 else -1) in
  assert ((List.sort edges_compare_func d.edges) = [
    { src = 1; char = Some 'a'; dst = 1 }; 
    { src = 1; char = Some 'b'; dst = 2 }; 
    { src = 2; char = Some 'a'; dst = 3 };
    { src = 2; char = Some 'b'; dst = 2 };
    { src = 3; char = Some 'a'; dst = 2 }; 
    { src = 3; char = Some 'b'; dst = 2 }
  ]);
  pass_test ();
  print_endline "pass test_nfa_as_dict";;

let test_dfa_from_dict () =
  let open DFA in
  let dfa1 = {
    start_state = 1;
    accepted_states = [2];
    edges = [
      { src = 1; char = 'a'; dst = 1 }; 
      { src = 1; char = 'b'; dst = 2 }; 
      { src = 2; char = 'a'; dst = 3 };
      { src = 2; char = 'b'; dst = 2 };
      { src = 3; char = 'a'; dst = 2 }; 
      { src = 3; char = 'b'; dst = 2 }
    ]
  } in
  let d = from_dict dfa1 in
  let expected = create
    (Hashset.of_list ['a'; 'b'])
    (transition_table [
      ((1,'a'), 1);
      ((1,'b'), 2);
      ((2,'a'), 3);
      ((2,'b'), 2);
      ((3,'a'), 2);
      ((3,'b'), 2)
    ])
    1
    (Accepted_List [2]) in
  assert (equal d expected);
  pass_test ();
  print_endline "pass test_dfa_from_dict";;

let test_nfa1_from_dict () =
  let open NFA in
  let nfa1 = {
    start_state = 1;
    accepted_states = [2];
    edges = [
      { src = 1; char = Some 'a'; dst = 1 };
      { src = 1; char = Some 'b'; dst = 2 };
      { src = 2; char = Some 'a'; dst = 3 };
      { src = 2; char = Some 'b'; dst = 2 };
      { src = 3; char = Some 'a'; dst = 2 };
      { src = 3; char = Some 'b'; dst = 2 }
    ]
  } in
  let d = from_dict nfa1 in
  assert (equal d (create
    (Hashset.of_list ['a'; 'b'])
    (transition_table [
      ((1, Some 'a'), [1]);
      ((1, Some 'b'), [2]);
      ((2, Some 'a'), [3]);
      ((2, Some 'b'), [2]);
      ((3, Some 'a'), [2]);
      ((3, Some 'b'), [2])
    ])
    1
    (Accepted_List [2])
  ));
  pass_test ();
  print_endline "pass test_nfa1_from_dict";;

let test_nfa_flatten () =
  let open NFA in
  let n1 = create
    (Hashset.of_list ['a'; 'b'])
    (transition_table [
      ((1, Some 'a'), [1]);
      ((1, Some 'b'), [2]);
      ((2, Some 'a'), [3]);
      ((2, Some 'b'), [2]);
      ((3, Some 'a'), [2]);
      ((3, Some 'b'), [2])
    ])
    1
    (Accepted_List [2]) in
  let n2 = (flatten (dfa_to_nfa (nfa_to_dfa n1))) in
  assert (DFA.is_equivalent_to (nfa_to_dfa n1) (nfa_to_dfa n2));
  pass_test ();
  print_endline "pass test_nfa_flatten";;

let test_nfa_remove_epsilon_transitions () =
  let open NFA in
  let n1 = create
    (Hashset.of_list ['a'; 'b'])
    (transition_table [
      ((1, Some 'a'), [1]);
      ((1, None),     [2]);
      ((2, Some 'a'), [4]);
      ((2, None),     [3]);
      ((2, Some 'b'), [2]);
      ((3, Some 'a'), [2]);
      ((3, Some 'b'), [2])
    ])
    1
    (Accepted_List [2]) in
  let n2 = create
    (Hashset.of_list ['a'; 'b'])
    (transition_table [
      ((1, Some 'a'), [1; 2; 4]);
      ((1, Some 'b'), [2]);
      ((2, Some 'a'), [2; 4]);
      ((2, Some 'b'), [2]);
      ((3, Some 'a'), [2]);
      ((3, Some 'b'), [2])
    ])
    1
    (Accepted_List [1; 2]) in
  assert (equal (remove_epsilon_transitions n1) n2);
  pass_test ();
  print_endline "pass test_nfa_remove_epsilon_transitions";;

let test_nfa_remove_epsilon_transitions_2 () =
  let open NFA in
  let n1 = remove_epsilon_transitions (create
    (Hashset.of_list [!la_on; !la_off; !t_s; !t_t; !t_c; !lb_on; !lb_off])
    (transition_edges_split [
      (0, None, 4);
      (4, Some !la_on, 6);
      (6, Some !t_s, 7);
      (7, None, 8);
      (8, None, 9);
      (8, None, 10);
      (10, Some !t_t, 33);
      (33, Some !la_off, 34);
      (34, None, 35);
      (35, None, 4);
      (9, Some !t_c, 36);
      (36, Some !lb_on, 38);
      (38, Some !t_s, 39);
      (39, None, 40);
      (40, None, 41);
      (41, Some !t_t, 43);
      (43, Some !lb_off, 44);
      (43, Some !la_off, 45);
      (44, Some !la_off, 48);
      (45, Some !lb_off, 46);
      (48, None, 47);
      (46, None, 47);
      (47, None, 4);
    ])
    0
    (Accepted_List [35; 47])) in
  let n2 = create
    (Hashset.of_list [!la_on; !la_off; !t_s; !t_t; !t_c; !lb_on; !lb_off])
    (transition_edges_split [
      (0, Some !la_on, 6);
      (6, Some !t_s, 7);
      (7, Some !t_c, 36);
      (7, Some !t_t, 33);
      (33, Some !la_off, 34);
      (34, Some !la_on, 6);
      (9, Some !t_c, 36);
      (36, Some !lb_on, 38);
      (38, Some !t_s, 39);
      (39, Some !t_t, 43);
      (43, Some !lb_off, 44);
      (43, Some !la_off, 45);
      (44, Some !la_off, 48);
      (45, Some !lb_off, 46);
      (48, Some !la_on, 6);
      (46, Some !la_on, 6);
    ])
    0
    (Accepted_List [34; 48; 46]) in
  assert ((List.sort compare (rev_list_of_stream (states n1))) = 
    (List.sort compare (rev_list_of_stream (states n2))));
  assert ((List.sort compare (rev_list_of_stream (stream_filter n1.accepted_states (states n1)))) = 
    (List.sort compare (rev_list_of_stream (stream_filter n2.accepted_states (states n2)))));
  Stream.iter 
    (fun st -> 
      assert ((List.sort compare (rev_list_of_stream (get_outgoing n1 st))) = 
      (List.sort compare (rev_list_of_stream (get_outgoing n2 st))))) 
    (states n1);
  assert (equal n1 n2);
  pass_test ();
  print_endline "pass test_nfa_remove_epsilon_transitions_2";;

let test_nfa_remove_sink_states () =
  let open NFA in
  let n1 = create
    (Hashset.of_list ['a'; 'b'])
    (transition_table [
      ((1, Some 'a'), [2]);
      ((2, Some 'a'), [3]);
      ((3, Some 'a'), [3]);
      ((3, Some 'b'), [3])
    ])
    1
    (Accepted_List [2]) in
  let n2 = create
    (Hashset.of_list ['a'; 'b'])
    (transition_table [
      ((1, Some 'a'), [2]);
      ((2, Some 'a'), [])
    ])
    1
    (Accepted_List [2]) in
  assert (equal (remove_sink_states n1) n2);
  pass_test ();
  print_endline "pass test_nfa_remove_sink_states";;

(* =============================== run tests =============================== *)
let main () = 
  test_fig1_2 ();
  test_shortest_string ();
  test_divergence_index ();
  test_fig1_4 ();
  test_button ();
  test_convert_dfa ();
  test_complement ();
  test_shuffle ();
  test_shuffle_2 ();
  test_sink ();
  test_led_and_button ();
  test_contains ();
  test_hello_world_not_empty ();
  test_gnfa_step1 ();
  test_gnfa_step2 ();
  test_gnfa_step3 ();
  test_gnfa_step4 ();
  test_gnfa_step5 ();
  test_nfa_concat ();
  test_eq_1 ();
  test_nfa_star ();
  test_step_algo ();
  test_transition_edges_split ();
  test_dfa_edges_1 ();
  test_dfa_iter_outgoing_1 ();
  test_nfa_edges_1 ();
  test_nfa_iter_outgoing_1 ();
  test_nfa_state_diagram_1 ();
  test_minimize ();
  test_subtract ();
  test_set_alphabet ();
  test_dfa_as_dict ();
  test_dfa_get_derivation ();
  test_nfa_get_derivation ();
  test_nfa_as_dict ();
  test_dfa_from_dict ();
  test_nfa1_from_dict ();
  test_nfa_flatten ();
  test_nfa_remove_epsilon_transitions ();
  test_nfa_remove_epsilon_transitions_2 ();
  test_nfa_remove_sink_states ();;

let () = 
  Printexc.record_backtrace true;
  let t = Unix.gettimeofday () in
  main ();
  let open Printf in
  printf "Passed tests: %d\n" !test_count;
  printf "Execution time: %fs\n" (Unix.gettimeofday () -. t);;