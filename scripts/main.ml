open Regular
open Cmdliner

let get_filename_without_extension file_path =
  let rec remove_directory = function
    | [filename] -> filename
    | hd :: tl -> remove_directory tl
    | [] -> failwith "empty file_path" in
  let filename = remove_directory (String.split_on_char '/' file_path) in
  List.hd (String.split_on_char '.' filename)

let main
  file_path
  input
  output
  state_type
  char_type
  dot
  log
  minimize
  slow
=
  let open Yojson.Basic.Util in
  let json = Yojson.Basic.from_file file_path in
  let filename_no_ext = get_filename_without_extension file_path in
  (* ===================== *)
  let give_output_dfa (dfa : (int, string) Regular.DFA.t) input_info =
    let out_str = 
      match output with
      | "json" -> Yojson.Basic.pretty_to_string (
        Parser.DFA.to_json dfa (fun st -> `Int st) (fun char -> `String char)
      )
      | "fsm" -> Parser.DFA.to_fsm dfa string_of_int (fun char -> char)
      | "string" -> DFA.to_string dfa string_of_int (fun char -> char)
      | _ -> failwith ("Mismatching output format: " ^ output) in
    let filename_out = "out_" ^ filename_no_ext ^ "." ^ (
      if output = "string" then "txt" else output
    ) in
    let out_channel = open_out filename_out in
    Printf.fprintf out_channel "%s\n" out_str;
    close_out out_channel;
    (if log then print_string input_info);
    if dot then Outputdot.from_dfa dfa ("out_" ^ filename_no_ext) in
  (* ===================== *)
  let give_output_nfa (nfa : (int, string) Regular.NFA.t) input_info =
    let out_str = 
      match output with
      | "json" -> Yojson.Basic.pretty_to_string (
        Parser.NFA.to_json nfa (fun st -> `Int st) (fun char -> `String char)
      )
      | "fsm" -> Parser.NFA.to_fsm nfa string_of_int (fun char -> char)
      | "string" -> NFA.to_string nfa string_of_int (fun char -> char)
      | _ -> failwith ("Mismatching output format: " ^ output) in
    let filename_out = "out_" ^ filename_no_ext ^ "." ^ (
      if output = "string" then "txt" else output
    ) in
    let out_channel = open_out filename_out in
    Printf.fprintf out_channel "%s\n" out_str;
    close_out out_channel;
    (if log then print_string input_info);
    if dot then Outputdot.from_nfa nfa ("out_" ^ filename_no_ext) in
  (* ===================== *)
  let give_output_bool b exp =
    let cons_str = Exp.Boolexp.string_of_cons exp in
    let bool_str = if b then "true" else "false" in
    Printf.printf "%s: %s" cons_str bool_str in
  (* ===================== *)
  let get_input (to_st, filter_st) (to_char, filter_char) = 
    match input with
    | "dfa" -> begin
      if minimize then begin
        if slow then (* no optimization *)
          let dfa = Parser.DFA.of_json json to_st filter_st to_char in
          let mini_dfa = DFA.minimize dfa in
          give_output_dfa mini_dfa (
            Printf.sprintf "Input: %d\nMinimized DFA: %d\n" 
            (DFA.size dfa) (DFA.size mini_dfa)
          )
        else (* optimize by removing sinks *)
          let nfa = Parser.NFA.of_json json to_st filter_st to_char in
          let nfa_sinkless = NFA.remove_sink_states nfa in
          let dfa = nfa_to_dfa nfa_sinkless in
          let mini_dfa = DFA.minimize dfa in
          give_output_dfa mini_dfa (
            Printf.sprintf "Input: %d\nNo sinks: %d\nDFA: %d\nMinimized DFA: %d\n" 
            (NFA.size nfa) (NFA.size nfa_sinkless) (DFA.size dfa) (DFA.size mini_dfa)
          )
      end else (* no minimization *)
        let dfa = Parser.DFA.of_json json to_st filter_st to_char in
        let dfa_flat = DFA.flatten dfa in
        give_output_dfa dfa_flat (Printf.sprintf "No minimization.\n DFA: %d\n" (DFA.size dfa))
    end
    | "ltl" -> begin
        let (vars, p) = Parser.LTL.parse json in
        let nfa = Parser.LTL.of_json json in
        Ltl.debug_ltl p vars;
        give_output_nfa nfa (Printf.sprintf "LTL: %s\n" (Ltl.ltl_to_string p))
      end
    | "nfa" -> begin
      if slow then
        let nfa = Parser.NFA.of_json json to_st filter_st to_char in
        let nfa_flat = NFA.flatten nfa in
        give_output_nfa nfa_flat (Printf.sprintf "NFA: %d" (NFA.size nfa))
      else (* remove sinks *)
        let nfa = Parser.NFA.of_json json to_st filter_st to_char in
        let nfa_sinkless = NFA.remove_sink_states nfa in
        let nfa_sinkless_flat = NFA.flatten nfa in
        give_output_nfa nfa_sinkless_flat (
          Printf.sprintf "NFA: %d\nNo sinks: %d\n" (NFA.size nfa) (NFA.size nfa_sinkless)
        )
    end
    | "dfa_exp" -> begin
      let dfa_exp =   
        let d = Parser.DFAexp.of_json json to_st filter_st to_char filter_char in
        if slow then d else Exp.DFAexp.optimize d in
      let dfa_exp_str = Yojson.Basic.pretty_to_string (
        Parser.DFAexp.to_json dfa_exp (fun st -> `Int st) (fun char -> `String char)
      ) in
      print_endline (if slow then "No optimization:" else "After optimization:");
      print_endline dfa_exp_str;
      let alp = Exp.DFAexp.get_alphabet dfa_exp in
      let dfa = Exp.DFAexp.eval alp dfa_exp in
      give_output_dfa dfa (Printf.sprintf "DFA: %d\n" (DFA.size dfa))
    end
    | "nfa_exp" -> begin
      let nfa_exp =   
        let n = Parser.NFAexp.of_json json to_st filter_st to_char filter_char in
        if slow then n else Exp.NFAexp.optimize n in
      let nfa_exp_str = Yojson.Basic.pretty_to_string (
        Parser.NFAexp.to_json nfa_exp (fun st -> `Int st) (fun char -> `String char)
      ) in
      print_endline (if slow then "No optimization:" else "After optimization:");
      print_endline nfa_exp_str;
      let alp = Exp.NFAexp.get_alphabet nfa_exp in
      let nfa = Exp.NFAexp.eval alp nfa_exp in
      give_output_nfa nfa (Printf.sprintf "NFA: %d\n" (NFA.size nfa))
    end
    | "bool_exp" -> begin
      let bool_exp =   
        let b = Parser.Boolexp.of_json json to_st filter_st to_char filter_char in
        if slow then b else Exp.Boolexp.optimize b in
      let bool_exp_str = Yojson.Basic.pretty_to_string (
        Parser.Boolexp.to_json bool_exp (fun st -> `Int st) (fun char -> `String char)
      ) in
      print_endline (if slow then "No optimization:" else "After optimization:");
      print_endline bool_exp_str;
      let alp = Exp.Boolexp.get_alphabet bool_exp in
      let bool = Exp.Boolexp.eval alp bool_exp in
      give_output_bool bool bool_exp
    end
    | _ -> failwith ("Mismatching input type: " ^ input) in
  (* ===================== *)
  (* functions that parse JSON types to OCaml data structures *)
  let st_int_handler = (to_int, filter_int) in
  let st_str_handler = (to_string, filter_string) in
  (* if char is integer, then convert to string *)
  let char_int_handler = (
    (fun x -> string_of_int (to_int x)),
    (fun x -> List.map string_of_int (filter_int x))
  ) in
  let char_str_handler = (to_string, filter_string) in
  (* ===================== *)
  match state_type with
  | "int" -> begin
    match char_type with
    | "int" -> get_input st_int_handler char_int_handler
    | "str" -> get_input st_int_handler char_str_handler
    | _ -> failwith ("Mismatching char type: " ^ state_type)
  end
  | "str" -> begin
    match char_type with
    | "int" -> get_input st_str_handler char_int_handler
    | "str" -> get_input st_str_handler char_str_handler
    | _ -> failwith ("Mismatching char type: " ^ state_type)
  end
  | _ -> failwith ("Mismatching state type: " ^ state_type)
(* ========================================================================= *)

let file_path =
  let doc = "Input JSON file." in
  Arg.(value & pos 0 string "" & info [] ~docv:"FILE_PATH" ~doc)

let input =
  let doc = "Input format. $(docv) = dfa/nfa/dfa_exp/nfa_exp/bool_exp/ltl" in
  Arg.(value & opt string "dfa" & info ["i"; "input"] ~docv:"INPUT" ~doc)

let input =
  let doc = "Input format. $(docv) = dfa/nfa/dfa_exp/nfa_exp/bool_exp/ltl" in
  Arg.(value & opt string "dfa" & info ["i"; "input"] ~docv:"INPUT" ~doc)

let output =
  let doc = "Output format. $(docv) = json/fsm/string" in
  Arg.(value & opt string "json" & info ["o"; "output"] ~docv:"OUTPUT" ~doc)

let state_type =
  let doc = "State/node type of input. $(docv) = int/str" in
  Arg.(value & opt string "int" & info ["s"; "state"] ~docv:"STATE" ~doc)

let char_type =
  let doc = "Char/edge type of input. $(docv) = int/str" in
  Arg.(value & opt string "str" & info ["c"; "char"] ~docv:"CHAR" ~doc)

let dot = 
  let doc = "Generate a dot file that can used by Graphviz to draw a graph." in
  Arg.(value & flag & info ["d"; "dot"] ~doc)

let log = 
  let doc = "Show input/output size." in
  Arg.(value & flag & info ["l"; "log"] ~doc)

let minimize = 
  let doc = "Minimize the DFA or remove sinks for NFA." in
  Arg.(value & flag & info ["m"; "minimize"] ~doc)

let slow = 
  let doc = "Disable optimization for DFA minimization or expression operations." in
  Arg.(value & flag & info ["slow"] ~doc)

let main_t = Term.(
    const main $
    file_path $
    input $
    output $
    state_type $
    char_type $
    dot $
    log $
    minimize $
    slow
  )

let info =
  let doc = "You can minimize a DFA and/or generate a dot file for visualization." in
  let man = [
    `S Manpage.s_bugs;
    `P "Please report bugs to https://gitlab.com/umb-svl/karakuri-ocaml or e-mail to <XuHuang.Lin001@umb.edu>."
  ] in
  Term.info "main" ~version:"0.9.1" ~doc ~exits:Term.default_exits ~man

let () = 
  (*Printexc.record_backtrace true;*)
  Term.exit @@ Term.eval (main_t, info)
