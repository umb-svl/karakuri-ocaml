
type ltl =
  | Bool of bool
  | Var of string
  | And of ltl * ltl
  | Or of ltl * ltl
  | Neg of ltl
  | Next of ltl
  | Until of ltl * ltl

let binop (p:string) (op:string) (q:string) : string =
  "(" ^ p ^ " " ^ op ^ " " ^ q ^ ")"

let unop (op:string) (p:string) : string =
  op ^ "(" ^ p ^ ")"

let rec ltl_to_string (l:ltl) : string =
  match l with
  | Bool b -> if b then "T" else "F"
  | Var x -> x
  | And (p, q) -> binop (ltl_to_string p) "/\\" (ltl_to_string q)
  | Or  (p, q) -> binop (ltl_to_string p) "/\\" (ltl_to_string q)
  | Neg p -> unop "~" (ltl_to_string p)
  | Next p -> unop "N" (ltl_to_string p)
  | Until (p, q) -> binop (ltl_to_string p) "U" (ltl_to_string q)

(*------ *)

type ltl_e =
  | Halt
  | Bool of bool
  | Var of string
  | NegVar of string
  | And of ltl_e * ltl_e
  | Or of ltl_e * ltl_e
  | Next of ltl_e
  | WeakNext of ltl_e
  | Until of ltl_e * ltl_e
  | Release of ltl_e * ltl_e

let rec list_or_e (ps:ltl_e list) =
  match ps with
  | [p] -> p
  | [] -> Bool false
  | p::ps -> Or (p, list_or_e ps)

let rec neg (p:ltl_e) : ltl_e =
  match p with
  | Bool b -> Bool (not b)
  | Var x -> NegVar x
  | NegVar x -> Var x
  | And (p, q) -> Or (neg p, neg q)
  | Or (p, q) -> And (neg p, neg q)
  | Next (Bool true) -> Halt
  | Next p -> WeakNext (neg p)
  | WeakNext p -> Next (neg p)
  | Halt -> Next (Bool true)
  | Release (p, q) -> Until (neg p, neg q)
  | Until (p, q) -> Release (neg p, neg q)

let rec ltl_to_e (p:ltl) =
  match p with
  | Bool b -> Bool b
  | Var x -> Var x
  | And (p, q) -> And (ltl_to_e p, ltl_to_e q)
  | Or (p, q) -> Or (ltl_to_e p, ltl_to_e q)
  | Neg p -> neg (ltl_to_e p)
  | Next p -> Next (ltl_to_e p)
  | Until (p, q) -> Until (ltl_to_e p, ltl_to_e q)

let rec ltl_e_to_string_ascii (l:ltl_e) : string =
  match l with
  | Bool b -> if b then "T" else "F"
  | Var x -> x
  | NegVar x -> "! " ^ x
  | And (p, q) -> binop (ltl_e_to_string_ascii p) " & " (ltl_e_to_string_ascii q)
  | Or  (p, q) -> binop (ltl_e_to_string_ascii p)  " | " (ltl_e_to_string_ascii q)
  | Next p -> unop "X" (ltl_e_to_string_ascii p)
  | WeakNext p -> unop "N" (ltl_e_to_string_ascii p)
  | Halt -> "H"
  | Until (p, q) -> binop (ltl_e_to_string_ascii p) " U " (ltl_e_to_string_ascii q)
  | Release (p, q) -> binop (ltl_e_to_string_ascii p) " R " (ltl_e_to_string_ascii q)

let rec ltl_e_to_string (l:ltl_e) : string =
  match l with
  | Bool b -> if b then "⊤" else "⊥"
  | Var x -> x
  | NegVar x -> "¬" ^ x
  | And (p, q) -> binop (ltl_e_to_string p) "∧" (ltl_e_to_string q)
  | Or  (p, q) -> binop (ltl_e_to_string p) "∨" (ltl_e_to_string q)
  | Next p -> unop "X" (ltl_e_to_string p)
  | WeakNext p -> unop "N" (ltl_e_to_string p)
  | Halt -> "H"
  | Until (p, q) -> binop (ltl_e_to_string p) "U" (ltl_e_to_string q)
  | Release (p, q) -> binop (ltl_e_to_string p) "R" (ltl_e_to_string q)

let rec step1 (l:ltl_e) (word:string list) : ltl_e option =
  match l with
  | Next _ -> None
  | WeakNext p -> None
  | Halt -> None
  | Bool _ -> None
  | Var x -> Some (Bool (List.mem x word))
  | NegVar x -> Some (Bool (not (List.mem x word)))
  | And (q, Bool true)
  | And (Bool true, q)
    -> Some q
  | And (q, Bool false)
  | And (Bool false, q)
    -> Some (Bool false)
  | And (p, q) ->
    begin match step1 p word with
    | Some p -> Some (And (p, q))
    | None ->
      begin match step1 q word with
      | Some q -> Some (And (p, q))
      | None -> None
      end
    end
  | Or (Bool false, q)
  | Or (q, Bool false) -> Some q
  | Or (Bool true, q)
  | Or (q, Bool true) -> Some (Bool true)
  | Or (p, q) ->
    begin match step1 p word with
    | Some p -> Some (Or (p, q))
    | None ->
      begin match step1 q word with
      | Some q -> Some (Or (p, q))
      | None -> None
      end
    end
  | Until (p, q) ->
    Some (Or (q, (And (p, Next (Until (p, q))))))
  | Release (p, q) ->
    Some (Or (And (p, q), And (q, WeakNext (Release (p, q)))))

let rec big_step p word =
  (* print_endline (ltl_e_to_string p); *)
  match step1 p word with
  | Some p ->
    big_step p word
  | None ->
    p

let rec advance (p:ltl_e) : ltl_e list =
  match p with
  | Halt -> []
  | Bool _ -> [p]
  | Until _
  | NegVar _
  | Var _
  | Release _ -> failwith ("Unexpected input: " ^ ltl_e_to_string p)
  | And (p, q) -> advance (neg p) @ advance (neg q)
  | Or (p, q) -> advance p @ advance q
  | WeakNext p -> [Halt; p]
  | Next p -> [p]

let transition (p:ltl_e) (word:string list) : ltl_e list =
  let ls = big_step p word |> advance in
  print_endline "***";
  List.iter (fun p ->
    print_endline (ltl_e_to_string p);
  ) ls;
  ls

(* --------------------------------------------------- *)

type ltl_g =
  | Bool of bool
  | Var of string
  | NegVar of string
  | Or of ltl_g * ltl_g
  | And of ltl_g * ltl_g
  | Next of ltl_e
  | WeakNext of ltl_e

let rec ltl_to_g (p:ltl_e) : ltl_g =
  match p with
  | Bool b -> Bool b
  | Var x  -> Var x
  | NegVar x -> NegVar x
  | Next p -> Next p
  | WeakNext p -> WeakNext p
  | Halt -> WeakNext (Bool false)
  | And (p, q) -> And (ltl_to_g p, ltl_to_g q)
  | Or (p, q) -> Or (ltl_to_g p, ltl_to_g q)
  | Until (p', q') -> Or (ltl_to_g q', And (ltl_to_g p', Next p))
  | Release (p', q') -> And (ltl_to_g q', Or (ltl_to_g p', WeakNext  p))

(* ------------------------------------------ *)

type lit_d =
  | Bool of bool
  | Var of string
  | NegVar of string
  | Next of ltl_e
  | WeakNext of ltl_e

type clause_d =
  | And of lit_d * clause_d
  | Lit of lit_d

let and_d (l:lit_d) (d:clause_d) : clause_d =
  match l, d with
  | Bool true, _ -> d
  | Bool false, _
  | _, Lit (Bool false)
    -> Lit (Bool false)
  | NegVar x, And (Var y, _) when x = y
    -> Lit (Bool false)
  | Var x, And (NegVar y, _) when x = y
    -> Lit (Bool false)
  | _, _ -> And (l, d)

let rec and_d_ex (a:clause_d) (b:clause_d) : clause_d =
  match a with
  | Lit l -> and_d l b
  | And (l, a) -> and_d_ex a (and_d l b)

type ltl_d =
  | Or of (ltl_d * ltl_d)
  | Clause of clause_d

let or_d (a:clause_d) (b:ltl_d) =
  match a, b with
  | Lit (Bool true), _ -> Clause (Lit (Bool true))
  | Lit (Bool false), _ -> b
  | _, _ -> Or (Clause a, b)

(* --------------------------------------- *)

let g_to_l (g:ltl_g) : lit_d option =
  match g with
  | Bool b -> Some (Bool b)
  | Var x -> Some (Var x)
  | NegVar x -> Some (NegVar x)
  | Next p -> Some (Next p)
  | WeakNext p -> Some (WeakNext p)
  | _ -> None

let rec g_to_c (g:ltl_g) : clause_d option =
  match g_to_l g with
  | Some l -> Some (Lit l)
  | None -> begin
    match g with
    | And (p, q) ->
      begin match g_to_c p, g_to_c q with
      | Some p, Some q -> Some (and_d_ex p q)
      | _, _ -> None
      end
    | _ -> None
  end

let rec add1 (l:lit_d) (p:ltl_d) : ltl_d =
  match p with
  | Or (p, q) -> Or (add1 l p, add1 l q)
  | Clause c -> Clause (and_d l c)

let rec add (c:clause_d) (d:ltl_d) =
  match c with
  | And (p, q) -> Or (add1 p d, add q d)
  | Lit c -> add1 c d

let rec mult (p:ltl_d) (q:ltl_d) =
  match p with
  | Or (p', q') -> mult p' (mult q' q)
  | Clause c -> add c q

let rec g_to_d (g:ltl_g) : ltl_d option =
  match g_to_l g with
  | Some l -> Some (Clause (Lit l))
  | None ->
    begin match g_to_c g with
    | Some c -> Some (Clause c)
    | None ->
      begin match g with
      | Or (p, q) ->
        begin match g_to_d p, g_to_d q with
        | Some p, Some q -> Some (Or (p, q))
        | _, _ -> None
        end
      | And (p, q) ->
        begin match g_to_d p, g_to_d q with
        | Some p, Some q -> Some (mult p q)
        | _, _ -> None
        end
      | _ -> None
      end
    end

(* ------------------------------------------------------------- *)

type lit_a =
  | Var of string
  | NegVar of string

type clause_a =
  | Next of lit_a list * ltl_e list
  | WeakNext of lit_a list * ltl_e list

type ltl_a = clause_a list


let add_lit (l:lit_a) (a:clause_a) =
  match a with
  | Next (ls, ns) -> Next (l::ls, ns)
  | WeakNext (ls, ns) -> WeakNext (l::ls, ns)

let add_next (p:ltl_e) (a:clause_a) =
  match a with
  | Next (ls, ns)
  | WeakNext (ls, ns) -> Next (ls, p::ns)

let add_weak_next (p:ltl_e) (a:clause_a) =
  match a with
  | Next (ls, ns) -> Next (ls, p::ns)
  | WeakNext (ls, ns) -> WeakNext (ls, p::ns)

let add_clause (a:clause_a) (b:clause_a) =
  match a, b with
  | Next (ns, ls), Next (ns', ls')
  | Next (ns, ls), WeakNext (ns', ls')
  | WeakNext (ns, ls), Next (ns', ls')
    -> Next (ns @ ns', ls @ ls')
  | WeakNext (ns, ls), WeakNext (ns', ls')
    -> WeakNext (ns @ ns', ls @ ls')

let d_lit_to_a (l:lit_d) : clause_a option =
  match l with
  | Bool true -> Some (WeakNext ([], []))
  | Bool false -> None
  | Var x -> Some (WeakNext ([Var x], []))
  | NegVar x -> Some (WeakNext ([NegVar x], []))
  | Next p -> Some (Next ([], [p]))
  | WeakNext p -> Some (WeakNext ([], [p]))

let rec d_clause_to_a (d:clause_d) : clause_a option =
  match d with
  | Lit l -> d_lit_to_a l
  | And (p, q) ->
    begin match d_lit_to_a p, d_clause_to_a q with
    | Some p, Some q -> Some (add_clause p q)
    | _, _ -> None
    end

let rec d_to_a (d:ltl_d) : ltl_a option =
  match d with
  | Clause c ->
    begin match d_clause_to_a c with
    | Some c -> Some [c]
    | None -> None
    end
  | Or (p, q) ->
    begin match d_to_a p, d_to_a q with
    | Some p, Some q -> Some (p @ q)
    | _, _ -> None
    end

let rec word_to_ltl (l:string list) : ltl_e =
  match l with
  | [x] -> Var x
  | x :: l -> And (Var x, word_to_ltl l)
  | [] -> Bool false

let word_to_string_ascii l : string =
  word_to_ltl l |> ltl_e_to_string_ascii

let word_to_string l : string =
  word_to_ltl l |> ltl_e_to_string

let rec words_to_string ls : string =
  match ls with
  | x :: l -> word_to_string x ^ ";" ^ words_to_string l
  | [] -> ""

let rec print_step_n p word =
  match step1 p word with
  | Some p ->
    print_endline ("⇔ " ^ word_to_string word ^ " ⊢ " ^ ltl_e_to_string p);
    print_step_n p word
  | None ->
    print_endline ("⇔ " ^ word_to_string word ^ " ⊢ " ^ ltl_e_to_string p);
    p

let rec is_final : ltl_e -> bool = function
  | Halt
  | Bool true -> true
  | Release _
  | NegVar _
  | Var _
  | Until _
  | Next _
  | WeakNext _ -> false
  | Bool false -> false
  | Or (p, q) -> is_final p && is_final q
  | And (p, q) -> is_final p && is_final q

type word_t = string list
type edge_t = ltl_e * word_t * ltl_e

type next_t = {
  next_known : ltl_e list;
  next_new : ltl_e list;
  next_edges : edge_t list;
}

let list_next (p:ltl_e) (n:next_t) (sigma:string list list) : next_t =
  (* print_endline ("list_next: " ^ ltl_e_to_string p); *)
  List.fold_right (fun (w:word_t) (n:next_t) ->
    List.fold_right (fun p' n ->
      let edge = (p, w, p') in
      if List.mem p' n.next_known then
        { n with next_edges = edge :: n.next_edges;}
      else
        {
          next_edges = edge :: n.next_edges;
          next_new = p' :: n.next_new;
          next_known = p' :: n.next_known
        }
    ) (transition p w) n
  ) sigma n

let list_all p sigma : edge_t list =
  let rec iter (n:next_t) =
    match n.next_new with
    | p :: to_visit ->
      let n = list_next p { n with next_new = to_visit } sigma in
      iter n
    | [] -> n.next_edges
  in
  iter { next_known = [p]; next_new = [p]; next_edges = []}
(*
let rec print_big_step p words =
  match words with
  | word :: words ->
    let p = print_step_n p word in
    let p' = advance p in
    print_endline ("Advance: " ^ ltl_e_to_string p ^ " → " ^ ltl_e_to_string p');
    print_endline "-------";
    print_big_step p' words
  | [] ->
    let p = print_step_n p [] in
    print_endline (ltl_e_to_string p);
    (if is_final p then
      print_endline "ACCEPT!"
    else print_endline "REJECTT!"
    );
    p
*)

let eventually p : ltl = Until (Bool true, p)
let eventually_e p : ltl_e = Until (Bool true, p)
let always p : ltl = Neg (eventually (Neg p))
let always_e (p: ltl_e) = neg (eventually_e (neg p))

let debug_ltl (p:ltl) (vars:string list) =
  let p = ltl_to_e p in
  let words = List.map (fun a -> [a]) vars in
  let edges = list_all p words in
  let nodes = List.fold_right (fun (p, w, p') ps ->
      let ps = if List.mem p' ps then
          ps
        else
          p'::ps
      in
      if List.mem p ps then ps else p::ps
    ) edges []
  in
  print_endline "digraph ltl {";
  List.iter (fun p ->
    print_endline ("node [shape=" ^ (if is_final p then "doublecircle" else "circle") ^ "];");
    print_endline ("\"" ^ ltl_e_to_string_ascii p ^ "\";");
  ) nodes;
  List.iter (fun (p, w, p') ->
    print_endline ("\"" ^ ltl_e_to_string_ascii p ^ "\" -> \"" ^ ltl_e_to_string_ascii p' ^ "\" [label=\"" ^ word_to_string_ascii w ^  "\"];")
  ) edges;
  print_endline "}"

(* let xor p q : ltl_e = Or (And (p, neg q), And (neg p, q)) *)
(*
let _ =
  let (p : ltl_e) = (Until ((Var "a"), Release (Var "b", Until (Var "c", Var "a")))) in
  (* let p = always (xor (Var "a") (Var "b")) in *)
  let words: string list list = [["c"; "b"; "a"]; ["a"; "b"]; ["b"; "c"]; ["a"; "c"]; ["c"]; ["a"]; ["b"]; []] in
  (* print_endline (words_to_string words ^ " ⊨ " ^ ltl_e_to_string p); *)
  (* print_big_step p words *)
  (* step_n p  [] *)
  let edges = list_all p words in
  let nodes = List.fold_right (fun (p, w, p') ps ->
      let ps = if List.mem p' ps then
          ps
        else
          p'::ps
      in
      if List.mem p ps then ps else p::ps
    ) edges []
  in
  print_endline "digraph ltl {";
  List.iter (fun p ->
    print_endline ("node [shape=" ^ (if is_final p then "doublecircle" else "circle") ^ "];");
    print_endline ("\"" ^ ltl_e_to_string_ascii p ^ "\";");
  ) nodes;
  List.iter (fun (p, w, p') ->
    print_endline ("\"" ^ ltl_e_to_string_ascii p ^ "\" -> \"" ^ ltl_e_to_string_ascii p' ^ "\" [label=\"" ^ word_to_string_ascii w ^  "\"];")
  ) edges;
  print_endline "}"
*)