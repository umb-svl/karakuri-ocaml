let rev_repeat_list list repeat =
  (* reversed but more efficient *)
  (*  # rev_repeat_list [1;2;3] 3;;
      - : int list = [3; 2; 1; 3; 2; 1; 3; 2; 1] *)
  let rec loop accum count =
    if count >= 1
    then loop (List.rev_append list accum) (count - 1)
    else accum in
  loop [] repeat;;

let product_of_lists ?(repeat=1) (args : 'A list list) =
  (* cartesian product, equivalent to a nested for-loop *)
  (*  # product_of_lists [['A';'B';'C';'D']; ['x';'y']];;
      - : char list list =
      [['A'; 'x']; ['A'; 'y']; ['B'; 'x']; ['B'; 'y']; ['C'; 'x']; 
      ['C'; 'y']; ['D'; 'x']; ['D'; 'y']]
      # product_of_lists ~repeat:3 [[0;1]];;
      - : char list list =
      [[0; 0; 0]; [0; 0; 1]; [0; 1; 0]; [0; 1; 1]; [1; 0; 0]; [1; 0; 1]; 
      [1; 1; 0]; [1; 1; 1]] *)
  let pools = rev_repeat_list args repeat in (* nm *)
  let result_ptr = ref [[]] in 
  let tmp_result_ptr = ref [] in
  let pools_iter_func pool = (* nm *)
    let pool_iter_func y = (* 0 ~ n *)
      let result_iter_func x = (* n^n *)
        tmp_result_ptr := (y::x) :: !tmp_result_ptr in 
      List.iter result_iter_func !result_ptr in 
    List.iter pool_iter_func pool;
    result_ptr := List.rev !tmp_result_ptr;
    tmp_result_ptr := [] in
  List.iter pools_iter_func pools;
  !result_ptr;;

let rev_list_remove_duplicates l =
  (* reversed but more efficient *)
  let set = Hashset.create (List.length l) in
  let rec loop list accum =
    match list with
    | hd :: tl ->
      if Hashset.mem set hd 
      then loop tl accum
      else (Hashset.add set hd; loop tl (hd :: accum))
    | [] -> accum in
  loop l [];;

let list_inter l1 l2 =
  (* output list in order of l1 *)
  let set2 = Hashset.of_list l2 in
  List.filter (fun x -> Hashset.mem set2 x) l1;;

let list_union l1 l2 =
  (* also remove duplicates, output list with random order *)
  let set = Hashset.create ((List.length l1) + (List.length l2)) in
  List.iter (fun x -> Hashset.add set x) l1;
  List.iter (fun x -> Hashset.add set x) l2;
  Hashset.to_list set;;

let rev_list_of_stream stream =
  (*  can't be used with an infinite stream
      the list is reversed, but more efficient *)
  let rec loop accum =
    match Stream.peek stream with
    | None -> accum
    | Some value -> begin
      Stream.junk stream;
      loop (value::accum)
    end in
  loop [];;

let stream_filter p stream =
  (*  do not supply:
      - p that never returns true for any element
      - an infinite stream *)
  let rec next i =
    match Stream.peek stream with
    | None -> None
    | Some value -> begin
      Stream.junk stream;
      if p value then Some value else next i
    end in
  Stream.from next;;

let hashtbl_get tbl key sink =
  match Hashtbl.find_opt tbl key with
  | Some x -> x
  | None -> sink;;

let hashtbl_equal ?(val_equal=(=)) h1 h2 =
  let open Hashtbl in
  if length h1 <> length h2 then false
  else begin
    try Hashtbl.iter (fun k v -> if not ((mem h2 k) && (val_equal v (find h2 k))) then raise Exit) h1; true
    with Stdlib.Exit -> false
  end;;
