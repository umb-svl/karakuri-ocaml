open Util

let init_size = ref 64 (* for creating Hashtbl and Hashset *)
(* ######################################################################### *)
(* DFA *)

type 'S accepted_type =
  | Accepted_Function of ('S -> bool)
  | Accepted_List of 'S list

let wrap_accepted_states = function
  (* user can supply a list or a function for accepted_states using accepted_type *)
  | Accepted_Function (func : 'S -> bool) -> func
  | Accepted_List (l : 'S list) ->
    let set = Hashset.of_list l in
    (fun x -> Hashset.mem set x)

let tag (st : 'S1) (iterable : 'S2 list) =
  (* list reversed but tail-recursive thus more efficient *)
  List.rev_map (fun x -> (st, x)) iterable

let equal_graph (g1 : 'S Hashset.t * ('S * 'S, 'A Hashset.t) Hashtbl.t) 
                (g2 : 'S Hashset.t * ('S * 'S, 'A Hashset.t) Hashtbl.t) =
  (* convert list to hash table for comparing in function equal *)
  let (nodes1, edges1) = g1 in
  let (nodes2, edges2) = g2 in
  if not (Hashset.equal nodes1 nodes2) then false 
  else begin
  hashtbl_equal ~val_equal:Hashset.equal edges1 edges2;
  end

class transition_lookup alphabet transitions =
  (* alphabet cannot have duplicates *)
  let alp_len = Hashset.cardinal alphabet in
  let grouped_transitions_init =
    let alpha_to_idx = Hashtbl.create alp_len in 
    Hashset.iteri (fun idx k -> Hashtbl.replace alpha_to_idx k idx) alphabet;
    let grouped_transitions = Array.init alp_len (fun _ -> Hashtbl.create !init_size) in
    let make_grouped_transitions (src, char) dst =
      let idx = Hashtbl.find alpha_to_idx char in
      Hashtbl.replace grouped_transitions.(idx) src dst in
    Hashtbl.iter make_grouped_transitions transitions;
    grouped_transitions in
  object (self)
    val mutable grouped_transitions = ( grouped_transitions_init : (int, int) Hashtbl.t array )
    val mutable cache = ( Hashtbl.create !init_size : (int * int, (int * int) list) Hashtbl.t )

    method get_edges (src, dst) =
      let return_list = ref [] in
      (* for some a in alphabet *)
      let grouped_transitions_iter_func tsx =
        (*  {tsx(p,a),tsx(q,a)} is marked
            new_src, new_dst = {tsx(p,a),tsx(q,a)}
            We sort the tuple because of symmetry *)
        let next_p = Hashtbl.find tsx src in
        let next_q = Hashtbl.find tsx dst in
        (* ignore transitions to same *)
        if next_p <> next_q 
        then return_list := ((min next_p next_q), (max next_p next_q)) :: !return_list in
      Array.iter grouped_transitions_iter_func grouped_transitions;
      !return_list

    method get_next_transitions edge =
      match Hashtbl.find_opt cache edge with
      | Some result -> result
      | None -> begin
        let result = self#get_edges edge in
        Hashtbl.replace cache edge result;
        result
      end

    method has_unmarked_edge edge edges =
      let tsx = self#get_next_transitions edge in
      let tsx_iter_func next_edge =
          if not (Hashset.mem edges next_edge)
          then begin
            (* This edge is no longer needed, so we can clear its cache *)
            Hashtbl.remove cache edge; 
            true
          end else false in
      List.exists tsx_iter_func tsx

    method get_next_to_mark edges =
      let return_val = ref None in
      let edges_iter_func edge =
        if self#has_unmarked_edge edge edges
        then begin
          return_val := Some edge;
          true
        end else false in
      ignore (Hashset.exists edges_iter_func edges);
      !return_val
  end

let minimize_transitions (alphabet : 'A Hashset.t) (state_count : int) 
                         (accepted_states : int Hashset.t) 
                         (transitions : (int * 'A, int) Hashtbl.t) =
  let tsx = new transition_lookup alphabet transitions in
  (*  https://www.cs.cornell.edu/courses/cs2800/2013fa/Handouts/minimization.pdf
      marked as soon as a reason is discovered why p and q are not equivalent
      Note: the table is symmetric, so we only store smaller-larger pairs *)
  let edges = 
    let rec make_edges src accum =
      if 0 > src-1 then accum else 
      let rec loop dst sub_accum =
        if src > dst then sub_accum
        else loop (dst-1) ((src-1, dst) :: sub_accum) in
      make_edges (src-1) (loop (state_count-1) accum) in
    make_edges state_count [] in
  (* Step 2: Mark{p,q} if 'p in F' XOR 'q in F' *)
  let is_unmarked (p, q) =
    (Hashset.mem accepted_states p) = (Hashset.mem accepted_states q) in
  (*  Unmarked entries are the ones where p in F <-> q in F
      Aka, Mark {p,q} == false *)
  let unmarked = Hashset.filter is_unmarked (Hashset.of_list edges) in
  (*  Step 3. Repeat the following until no more changes occur: if there exists
      a pair{p,q} in next_to_mark then mark{p,q}. *)
  let rec loop () =
    match tsx#get_next_to_mark unmarked with
    | None -> ()
    | Some pair -> (Hashset.remove unmarked pair; loop ()) in
  loop ();
  (*  Finally we are ready to create the new automaton
      p = q iff {p,q} unmarked
      Unmarked means equivalent, if there are no equivalence classes
      Then, there is nothing to minimize *)
  if (Hashset.cardinal unmarked) = 0 then transitions else
  (*  Now we know which states are different
      We build an array, such that for each state we know its
      equivalent ancestor (self-loop means no other is same) *)
  let states = Array.init state_count (fun i -> i) in
  let edges_iter_func pq =
    let pq_same = Hashset.mem unmarked pq in
    if pq_same
    then begin
      let (small, big) =
        let (p, q) = pq in
        if p > q then (q, p) else pq in
      states.(big) <- small
    end in
  List.iter edges_iter_func edges;
  (* Given a state, returns the unique ancestor of each state *)
  let get_state st =
    (*  Jump through list to get ancestor
        Keep around the original state, so that we can backtrack and update
        The root ancestor for all *)
    let old_st = st in
    let st = 
      let rec loop s =
        if states.(s) <> s then loop states.(s) else s in
      loop st in
    (* Cache the new root ancestor *)
    let rec loop old_st parent_st =
      if parent_st <> old_st
      then begin
        if parent_st = st then ()
        else begin
          states.(old_st) <- st;
          loop parent_st states.(old_st)
        end
      end else () in
    loop old_st states.(old_st);
    st in
  let new_transitions = Hashtbl.create !init_size in
  let transitions_iter_func (src, char) dst =
    (* For each transition, only store the canonical state *)
    let src = get_state src in
    let dst = get_state dst in
    Hashtbl.replace new_transitions (src, char) dst in
  Hashtbl.iter transitions_iter_func transitions;
  new_transitions

module DFA = struct
  type ('S, 'A) t = { 
    alphabet : 'A Hashset.t;
    transition_func : 'S -> 'A -> 'S;
    start_state : 'S;
    accepted_states : 'S -> bool }
  type ('S, 'A) edge = { src : 'S; char : 'A; dst : 'S }
  type ('S, 'A) dict = { 
    start_state : 'S; 
    accepted_states : 'S list; 
    edges : ('S, 'A) edge list }

  let create (alphabet : 'A Hashset.t) 
             (transition_func : 'S -> 'A -> 'S) 
             (start_state : 'S) 
             (accepted_states : 'S accepted_type) = 
    { alphabet = alphabet;
      transition_func = transition_func;
      start_state = start_state;
      accepted_states = (wrap_accepted_states accepted_states) }

  let product (dfa1 : ('S1, 'A) t) (dfa2 : ('S2, 'A) t) 
              (accepted_states : ('S1 * 'S2) accepted_type) =
    (* raise an exception if DFAs have different alphabets *)
    assert (Hashset.equal dfa1.alphabet dfa2.alphabet);
    let tsx1 = dfa1.transition_func in
    let tsx2 = dfa2.transition_func in
    let transition (q1, q2) a = begin
      ((tsx1 q1 a), (tsx2 q2 a)) end in
    create dfa1.alphabet
           transition
           (dfa1.start_state, dfa2.start_state)
           accepted_states

  let union (dfa1 : ('S1, 'A) t) (dfa2 : ('S2, 'A) t) =
    let acc1 = dfa1.accepted_states in
    let acc2 = dfa2.accepted_states in
    let is_final (q1, q2) =
      (acc1 q1) || (acc2 q2) in
    product dfa1 dfa2 (Accepted_Function is_final)

  let intersection (dfa1 : ('S1, 'A) t) (dfa2 : ('S2, 'A) t) =
    let acc1 = dfa1.accepted_states in
    let acc2 = dfa2.accepted_states in
    let is_final (q1, q2) =
      (acc1 q1) && (acc2 q2) in
    product dfa1 dfa2 (Accepted_Function is_final)

  let subtract (dfa1 : ('S1, 'A) t) (dfa2 : ('S2, 'A) t) =
    (* We use a direct encoding of subtraction to optimize performance *)
    let acc1 = dfa1.accepted_states in
    let acc2 = dfa2.accepted_states in
    let is_final (q1, q2) =
      (acc1 q1) && (not (acc2 q2)) in
    product dfa1 dfa2 (Accepted_Function is_final)

  let complement (dfa : ('S, 'A) t) =
    let is_final = dfa.accepted_states in
    create dfa.alphabet 
           dfa.transition_func 
           dfa.start_state 
           (Accepted_Function (fun x -> not (is_final x)))

  (* stream ================================================================ *)
  let get_outgoing (dfa : ('S, 'A) t) (st : 'S) =
    let dfa_alp_stream = Hashset.to_stream dfa.alphabet in
    let f i = 
      match Stream.peek dfa_alp_stream with
      | None -> None
      | Some char -> begin
        Stream.junk dfa_alp_stream;
        Some (char, dfa.transition_func st char)
      end in
    Stream.from f

  let edges (dfa : ('S, 'A) t) =
    let visited_nodes = Hashset.create !init_size in
    let to_visit = Queue.create () in
    Queue.add dfa.start_state to_visit;
    (* initialize pointers with dummies *)
    let dfa_src_outgoing_stream_ptr = ref (Stream.from (fun _ -> None)) in
    let src_ptr = ref dfa.start_state in
    (* return a stream that streams while to_visit isn't empty *)
    let rec f i =
      match Stream.peek !dfa_src_outgoing_stream_ptr with
      | None ->
        (* get next node when finished with previous node *)
        if Queue.is_empty to_visit then None else
        let src = Queue.pop to_visit in
        if Hashset.mem visited_nodes src then f i
        else begin
          (* new node *)
          Hashset.add visited_nodes src;
          dfa_src_outgoing_stream_ptr := get_outgoing dfa src;
          src_ptr := src;
          f i
        end
      | Some (char, dst) -> begin
        Stream.junk !dfa_src_outgoing_stream_ptr;
        (* stream outgoing edges of current node *)
        (if not (Hashset.mem visited_nodes dst)
        then Queue.add dst to_visit);
        Some (!src_ptr, char, dst)
      end in
    Stream.from f

  let iter_outgoing (dfa : ('S, 'A) t) =
    let last_src_opt_ptr = ref None in
    let outs_ptr = ref [] in
    let dfa_edges_stream = edges dfa in
    let rec f i =
      match Stream.peek dfa_edges_stream with
      | Some (src, char, dst) -> begin
        Stream.junk dfa_edges_stream;
        let last_src = match !last_src_opt_ptr with
          | None -> (last_src_opt_ptr := Some src; src) (* only on the first iteration *)
          | Some last_src -> last_src in 
        if src <> last_src
        then begin 
          last_src_opt_ptr := Some src;
          let outs = !outs_ptr in
          outs_ptr := [(char, dst)];
          Some (last_src, outs)
        end else begin
          outs_ptr := (char, dst) :: !outs_ptr; 
          f i
        end
      end
      | None -> begin
        (* edges stream drains, so about to end the loop *)
        match !last_src_opt_ptr with
        | None -> None
        | Some last_src -> begin
          last_src_opt_ptr := None;
          Some (last_src, !outs_ptr) 
        end
      end in
    Stream.from f

  let states (dfa : ('S, 'A) t) =
    let dfa_edges_stream = edges dfa in
    let last_src_opt_ptr = ref None in
    let rec f i =
      match Stream.peek dfa_edges_stream with
      | None -> None
      | Some (src, _, _) -> begin
        Stream.junk dfa_edges_stream;
        let src_opt = Some src in
        if src_opt <> !last_src_opt_ptr
        then begin 
          last_src_opt_ptr := src_opt;
          src_opt
        end else f i
      end in
    Stream.from f

  let end_states (dfa : ('S, 'A) t) =
    stream_filter dfa.accepted_states (states dfa)

  (* ======================================================================= *)
  let transition_table_of_hashtbl ?sink (tsx : ('S * 'A, 'S) Hashtbl.t) = 
    match sink with
    | None -> (fun st i -> Hashtbl.find tsx (st, i))
    | Some sink_val -> (fun st i -> hashtbl_get tsx (st, i) sink_val)

  let transition_table ?sink (table : (('S * 'A) * 'S) list) = 
    let tsx = Hashtbl.create !init_size in
    List.iter (fun (k, v) -> Hashtbl.replace tsx k v) table;
    match sink with
    | None -> transition_table_of_hashtbl tsx
    | Some sink_val -> transition_table_of_hashtbl ~sink:sink_val tsx

  let flatten ?(minimize=false) (dfa : ('S, 'A) t) =
    (*  Flatten returns a new DFA that caches all of its transitions,
        essentially improving time but reducing space. *)
    let state_to_id = Hashtbl.create !init_size in
    let get_state_id st =
      match Hashtbl.find_opt state_to_id st with
      | Some st_id -> st_id
      | None ->
        let st_id = Hashtbl.length state_to_id in
        Hashtbl.replace state_to_id st st_id;
        st_id in
    let transitions_ptr = ref (Hashtbl.create !init_size) in
    let accepted_states_ptr = ref [] in
    let dfa_iter_outgoing_iter_func (src, out) =
      let src_id = get_state_id src in
      (if dfa.accepted_states src
      then accepted_states_ptr := src_id :: !accepted_states_ptr);
      let out_iter_func (char, dst) =
        let dst_id = get_state_id dst in
        Hashtbl.replace !transitions_ptr (src_id, char) dst_id in
      List.iter out_iter_func out in
    Stream.iter dfa_iter_outgoing_iter_func (iter_outgoing dfa);
    begin
      if minimize
      then transitions_ptr := minimize_transitions 
        dfa.alphabet 
        (Hashtbl.length state_to_id)
        (Hashset.of_list !accepted_states_ptr)
        !transitions_ptr
    end;
    create dfa.alphabet
           (transition_table_of_hashtbl !transitions_ptr)
           (Hashtbl.find state_to_id dfa.start_state)
           (Accepted_List !accepted_states_ptr)

  let minimize (dfa : ('S, 'A) t) =
    flatten ~minimize:true dfa

  let set_alphabet (dfa : (int, 'A) t) (alphabet : 'A Hashset.t) =
    (*  Creates a new DFA that uses the same transition function, but a new
        alphabet. Essentially, this operation performs intersects the dfa
        alphabet with the given alphabet. *)
    let tsx = dfa.transition_func in
    let alpha = Hashset.inter dfa.alphabet alphabet in
    let transition_func (idx, q) a =
      if idx = 0 || not (Hashset.mem alpha a) 
      then (0, 0) 
      else (1, tsx q a) in
    let acc = dfa.accepted_states in
    let is_final (idx, q) =
      (idx = 1) && (acc q) in
    create alphabet
           transition_func
           (1, dfa.start_state)
           (Accepted_Function is_final)

  let get_minimized_graph (dfa : ('S, 'A) t) =
    let edges = Hashtbl.create !init_size in
    let nodes = Hashset.create !init_size in
    let dfa_iter_outgoing_iter_func (src, out) =
      Hashset.add nodes src;
      let out_iter_func (char, dst) =
        let src_dst = (src, dst) in
        let chars = match Hashtbl.find_opt edges src_dst with
          | Some chars -> chars
          | None -> begin
            let chars = Hashset.create !init_size in
            Hashtbl.replace edges src_dst chars;
            chars
          end in
        Hashset.add chars char in
      List.iter out_iter_func out in
    Stream.iter dfa_iter_outgoing_iter_func (iter_outgoing dfa);
    (nodes, edges)

  let as_graph =
    get_minimized_graph

  let get_full_graph (dfa : ('S, 'A) t) =
    let edges = Hashtbl.create !init_size in
    let dfa_states_list_ptr = ref [] in
    let dfa_states_iter_func src =
      dfa_states_list_ptr := src :: !dfa_states_list_ptr; (* list of stream *)
      let dfa_alp_iter_func char =
        let dst = dfa.transition_func src char in
        let kv = (src, dst) in
        match Hashtbl.find_opt edges kv with
        | None -> Hashtbl.replace edges kv [char]
        | Some chars -> Hashtbl.replace edges kv (char :: chars) in
      Hashset.iter dfa_alp_iter_func dfa.alphabet in
    Stream.iter dfa_states_iter_func (states dfa);
    (!dfa_states_list_ptr, edges)

  let find_shortest_path (dfa : ('S, 'A) t) (find_state : 'S -> bool) =
    (* Returns the shortest path according to some state condition. *)
    if find_state dfa.start_state then Some [] else
    let visited = Hashset.of_list ~init_size:!init_size [dfa.start_state] in
    (*  Perform a breadth-first visit, while ensuring we don't visit
        The same node more than once *)
    let result = ref None in
    let rec loop to_process =
      if to_process <> []
      then begin
        let next_frontier = ref [] in
        let to_process_iter_func (seq, src) =
          let dfa_alp_iter_func c =
            let next_st = dfa.transition_func src c in
            if Hashset.mem visited next_st 
            then false (* continue alphabet iter loop *)
            else begin
              let next_seq = c :: seq in
              if find_state next_st
              then begin
                (* Found the shortest string *)
                result := Some (List.rev next_seq); (* set return value *)
                true (* break alphabet and to_process iter loop *)
              end else begin
                Hashset.add visited next_st;
                next_frontier := (next_seq, next_st) :: !next_frontier;
                false (* iter loop *)
              end
            end in
          Hashset.exists dfa_alp_iter_func dfa.alphabet in
        if List.exists to_process_iter_func to_process 
        then () (* break loop *)
        else loop !next_frontier
      end in
    loop [([], dfa.start_state)];
    !result
      
  let get_shortest_string (dfa : ('S, 'A) t) =
    (* Returns a string with the shortest length recognizes by this automaton. *)
    find_shortest_path dfa dfa.accepted_states

  let get_derivation (dfa : ('S, 'A) t) (word : 'A list) =
    (*  Returns the derivation path for a given word.
        For each letter of the word, yields the next state.
        Does not yield the initial state. *)
    let rec loop accum st = function
      | c :: rest_word ->
        let next_st = dfa.transition_func st c in
        loop (next_st :: accum) next_st rest_word
      | [] -> accum in
    List.rev (loop [] dfa.start_state word)

  let as_dict ?(is_flatten=true) (dfa : (int, 'A) t) =
    (*  type of state must be int, 
        if want state type to be polymorphic, 
        need to write a same function w/o flatten option *)
    (*  Converts this DFA into a dfa_dict type.
        The alphabet must be serializable. *)
    let dfa = if is_flatten then flatten dfa else dfa in
    let edges = ref [] in
    let dfa_as_graph_edges = snd (as_graph dfa) in
    let dfa_as_graph_edges_iter_func (src, dst) chars =
      let chars_iter_func char =
        edges := { src = src; char = char; dst = dst } :: !edges in
      Hashset.iter chars_iter_func chars in
    Hashtbl.iter dfa_as_graph_edges_iter_func dfa_as_graph_edges;
    { start_state = dfa.start_state; 
      accepted_states = (rev_list_of_stream (end_states dfa)); 
      edges = !edges }

  let from_dict ?alphabet (dfa : ('S, 'A) dict) =
    (* Creates a DFA from a dictionary-based DFA (see `as_dict`). *)
    let alphabet = match alphabet with
      | Some alp -> alp
      | None -> begin (* duplicates will be removed in the create function *)
        let alp = Hashset.create !init_size in
        List.iter (fun edge -> Hashset.add alp edge.char) dfa.edges;
        alp
      end in
    let tbl = Hashtbl.create !init_size in
    let dfa_edges_iter_func edge =
      let src_char = (edge.src, edge.char) in
      (* raise an exception if duplicated edge *)
      assert (not (Hashtbl.mem tbl src_char));
      Hashtbl.replace tbl src_char edge.dst in
    List.iter dfa_edges_iter_func dfa.edges;
    create alphabet
           (transition_table_of_hashtbl tbl)
           dfa.start_state
           (Accepted_List dfa.accepted_states)

  let size (dfa : ('S, 'A) t) =
    (* Returns how many states the DFA contains *)
    let dfa_states_stream = states dfa in
    let rec junk_all () =
      match Stream.peek dfa_states_stream with
      | None -> ()
      | Some _ -> (Stream.junk dfa_states_stream; junk_all ()) in
    junk_all ();
    Stream.count dfa_states_stream
    
  let to_string (dfa : ('S, 'A) t) 
                (str_of_st : 'S -> string) 
                (str_of_char : 'A -> string) =
    (* requires string conversion functions for states and alphabet to be given *)
    let open Buffer in
    let open Printf in
    let data = create !init_size in
    let transitions = snd (as_graph dfa) in
    (* start_state *)
    add_string data (sprintf "DFA({start_state: %s, accepted_states: [" (str_of_st dfa.start_state));
    (* accepted_states *)
    let is_fst_st_iter = ref true in
    let end_states_iter_func st =
      (if !is_fst_st_iter then is_fst_st_iter := false else add_string data ", ");
      add_string data (str_of_st st) in
    Stream.iter end_states_iter_func (end_states dfa);
    add_string data "], transitions: {";
    (* transitions *)
    let is_fst_tran_iter = ref true in
    let transitions_iter_func (src, dst) chars =
      (if !is_fst_tran_iter then is_fst_tran_iter := false else add_string data ", ");
      add_string data (sprintf "(%s, %s): {" (str_of_st src) (str_of_st dst));
      let is_fst_char_iter = ref true in
      let chars_iter_func char =
        (if !is_fst_char_iter then is_fst_char_iter := false else add_string data ", ");
        add_string data (str_of_char char) in
      Hashset.iter chars_iter_func chars;
      add_string data "}" in
    Hashtbl.iter transitions_iter_func transitions;
    add_string data "}})";
    contents data

  (* ======================================================================= *)
  let accepts (dfa : ('S, 'A) t) (inputs : 'A list) =
    let st = 
      let rec loop st more_input =
        match more_input with
        | i :: rest_input -> 
          loop (dfa.transition_func st i) rest_input
        | [] -> st in
      loop dfa.start_state inputs in
    dfa.accepted_states st
    
  let is_sink (dfa : ('S, 'A) t) (st : 'S) =
    (* Tests if a state is a sink node (only contains self loops) *)
    if dfa.accepted_states st then false else
    let out = Hashset.create !init_size in
    let dfa_st_outgoing_iter_func (_, st) =
      Hashset.add out st in
    Stream.iter dfa_st_outgoing_iter_func (get_outgoing dfa st);
    ((Hashset.cardinal out) = 1) && (Hashset.mem out st)
  
  let is_empty (dfa : ('S, 'A) t) = 
    match Stream.peek (end_states dfa) with
    | None -> true
    | Some _ -> false

  let contains (dfa1 : ('S1, 'A) t) (dfa2 : ('S2, 'A) t) =
    is_empty (subtract dfa2 dfa1)

  let is_equivalent_to (dfa1 : ('S1, 'A) t) (dfa2 : ('S2, 'A) t) =
    (contains dfa1 dfa2) && (contains dfa2 dfa1)

  let equal (dfa1 : ('S, 'A) t) (dfa2 : ('S, 'A) t) =
    (*  cannot compare DFAs with different state or alphabet types,
        do so will raise an exception *)
    dfa1.start_state = dfa2.start_state &&
    (Hashset.equal (Hashset.of_stream (end_states dfa1)) (Hashset.of_stream (end_states dfa2))) &&
    equal_graph (as_graph dfa1) (as_graph dfa2)
  
  let get_divergence_index (dfa : ('S, 'A) t) (chars : 'A list) =
    (*  
      If a string is rejected, we may want to know what is the shortest
      substring that is accepted by the automaton. The algorithm expects
      a *minimized* automaton.

      Suppose the automaton only accepts '0*' and we want to know the
      divergence index of string '01', the divergence index should be 1,
      because it is the first index that has no further possibilities,
      whichever string you extend it (eg, 010 fails, 011 fails, 0110 fails,
      and so on).

      Usage example: see test_divergence_index in tests/test_regular.ml
    *)
    let idx = 0 in
    let st = dfa.start_state in
    if is_sink dfa st 
    then (if dfa.accepted_states st then None else Some idx) else 
    let rec loop st idx = function
      | i :: rest -> 
        let st = dfa.transition_func st i in
        if is_sink dfa st 
        then (if dfa.accepted_states st then None else Some idx)
        else loop st (idx + 1) rest
      | [] -> if dfa.accepted_states st then None else Some (idx - 1) in
    loop st idx chars

  (* ======================================================================= *)
  let make_nil (alphabet : 'A Hashset.t) =
    create 
      alphabet 
      (fun q a -> 1) 
      0 
      (Accepted_Function (fun x -> x = 0))

  let make_char (alphabet : 'A Hashset.t) (char : 'A) =
    (* raise an exception if the given char isn't in the given alphabet *)
    assert (Hashset.mem alphabet char);
    create 
      alphabet 
      (fun q a -> if q = 0 && a = char then 1 else 2) 
      0 
      (Accepted_Function (fun x -> x = 1))

  let make_void (alphabet : 'A Hashset.t) =
    create 
      alphabet 
      (fun q _ -> q) 
      0 
      (Accepted_List [])

  let make_all (alphabet : 'A Hashset.t) =
    create
      alphabet
      (fun q _ -> 0)
      0
      (Accepted_Function (fun x -> true))
end

let sample ?bound alphabet =
  (* Sample all words up to a certain bound. *)
  let current_list_ptr = ref [] in
  let count_ptr = ref 0 in
  let pop () =
    match !current_list_ptr with
    | hd :: tl -> current_list_ptr := tl; Some hd
    | [] -> None in
  let rec f i =
    match pop () with
    | Some v -> Some v
    | None -> 
      (* update *)
      current_list_ptr := product_of_lists ~repeat:!count_ptr [alphabet]; 
      count_ptr := !count_ptr + 1; 
      match bound with
      | None -> f i
      | Some b -> if !count_ptr > b then None else f i in
  Stream.from f

(* ######################################################################### *)
(* NFA *)

let multi_transition (transition_func : 'S -> 'A option -> 'S list) 
                     (states : 'S Hashset.t)
                     (char : 'A option) =
  let new_states = Hashset.create !init_size in 
  let states_iter_func st =
    List.iter (fun st -> Hashset.add new_states st) (transition_func st char) in 
  Hashset.iter states_iter_func states;
  new_states

let epsilon (transition_func : 'S -> 'A option -> 'S list) (states : 'S Hashset.t) =
  let states = Hashset.copy states in
  let rec loop () =
    let count = Hashset.cardinal states in 
    Hashset.update states (multi_transition transition_func states None);
    if count = (Hashset.cardinal states) then states else loop () in 
  loop ()

type 'S start_state =
  | StarState_Init
  | StarState_Other of 'S

type ('S1, 'S2) concat_state =
  | ConcatState_Left of 'S1
  | ConcatState_Right of 'S2

let rec derivation ?parent (s : 'S) =
  (* Used in NFA.get_derivations to obtain how each transition was performed. *)
  object (self)
    val mutable state = s
    val mutable parent = parent
    method state = state
    method parent = parent
    method link st = derivation ~parent:self st
    method iter f =
      match parent with
      | None -> ()
      | Some parent -> (parent#iter f; f state)
    method to_list =
      let list_ptr = ref [] in
      self#iter (fun st -> list_ptr := st :: !list_ptr);
      (List.rev !list_ptr)
  end

module NFA = struct
  type ('S, 'A) t = { 
    alphabet : 'A Hashset.t;
    transition_func : 'S -> 'A option -> 'S list;
    start_state : 'S;
    accepted_states : 'S -> bool }
  type ('S, 'A) edge = { src : 'S; char : 'A option; dst : 'S }
  type ('S, 'A) dict = { 
    start_state : 'S; 
    accepted_states : 'S list; 
    edges : ('S, 'A) edge list }

  let create (alphabet : 'A Hashset.t) 
             (transition_func : 'S -> 'A option -> 'S list) 
             (start_state : 'S) 
             (accepted_states : 'S accepted_type) = 
    { alphabet = alphabet;
      transition_func = transition_func;
      start_state = start_state;
      accepted_states = (wrap_accepted_states accepted_states) }

  let self_multi_transition (nfa : ('S, 'A) t) (states : 'S Hashset.t) (input : 'A option) =
    multi_transition nfa.transition_func states input

  let self_epsilon (nfa : ('S, 'A) t) (states : 'S Hashset.t) = 
    epsilon nfa.transition_func states
  
  let shuffle (nfa1 : ('S1, 'A) t) (nfa2 : ('S2, 'A) t) =
    let transition (st1, st2) a_opt =
      let result = Hashset.create !init_size in 
      begin
        match a_opt with
        | None -> begin
          List.iter (fun st1 -> Hashset.add result (st1, st2)) (nfa1.transition_func st1 a_opt);
          List.iter (fun st2 -> Hashset.add result (st1, st2)) (nfa2.transition_func st2 a_opt)
        end
        | Some a when Hashset.mem nfa1.alphabet a ->
          List.iter (fun st1 -> Hashset.add result (st1, st2)) (nfa1.transition_func st1 a_opt)
        | Some a when Hashset.mem nfa2.alphabet a ->
          List.iter (fun st2 -> Hashset.add result (st1, st2)) (nfa2.transition_func st2 a_opt)
        | Some a -> failwith "char a not found in both alphabets"
      end;
      (Hashset.to_list result) in
    let is_final (st1, st2) =
      nfa1.accepted_states st1 || nfa2.accepted_states st2 in 
    create 
      (Hashset.union nfa1.alphabet nfa2.alphabet) 
      transition 
      (nfa1.start_state, nfa2.start_state) 
      (Accepted_Function is_final)

  let union (nfa1 : (int, 'A) t) (nfa2 : (int, 'A) t) =
    (* state type must be int because the type of start_state of return NFA is (_ * int) *)
    let tsx = [| nfa1.transition_func; nfa2.transition_func |] in 
    let fin = [| nfa1.accepted_states; nfa2.accepted_states |] in 
    let transition (idx, st1) a =
      if idx = 2
      then (if a = None then [(0, nfa1.start_state); (1, nfa2.start_state)] else [])
      else tag idx (tsx.(idx) st1 a) in (* type: ignore *)
    let is_final (idx, st1) =
      if idx = 2 then false else fin.(idx) st1 in (* type: ignore *)
    create 
      (Hashset.union nfa1.alphabet nfa2.alphabet) 
      transition (2, 0) 
      (Accepted_Function is_final)

  let concat (nfa1 : ('S1, 'A) t) (nfa2 : ('S2, 'A) t) =
    let s2 = ConcatState_Right nfa2.start_state in
    let start_right = [s2] in 
    let transition st a =
      (* Add an extra edge *)
      match st with
      | ConcatState_Left s1 -> 
        let result = List.map (fun st -> ConcatState_Left st) (nfa1.transition_func s1 a) in 
        if nfa1.accepted_states s1 && a = None
        then list_union result start_right
        else result
      | ConcatState_Right s2 -> 
        List.map (fun st -> ConcatState_Right st) (nfa2.transition_func s2 a) in 
    let is_final st =
      match st with
      | ConcatState_Left _ -> false
      | ConcatState_Right state -> nfa2.accepted_states state in 
    create 
      (Hashset.union nfa1.alphabet nfa2.alphabet) 
      transition 
      (ConcatState_Left nfa1.start_state) 
      (Accepted_Function is_final)

  let star (nfa : ('S, 'A) t) =
    let init = StarState_Init in 
    let start = StarState_Other nfa.start_state in
    let q1 = [start] in
    let empty = [] in 
    let transition st a =
      match st with
      | StarState_Init -> if a = None then q1 else empty
      | StarState_Other state ->
        let result = List.rev_map (fun st -> StarState_Other st) (nfa.transition_func state a) in 
        if a = None && nfa.accepted_states state then init :: result else result in 
    create nfa.alphabet transition init (Accepted_List [init])

  (* stream ================================================================ *)
  let get_outgoing (nfa : ('S, 'A) t) (node : 'S) =
    let nfa_alp_stream = Hashset.to_stream nfa.alphabet in
    let is_done = ref false in
    let f i = 
      match Stream.peek nfa_alp_stream with
      | Some char -> begin
        Stream.junk nfa_alp_stream;
        Some (Some char, nfa.transition_func node (Some char))
      end
      | None ->
        if !is_done then None 
        else (is_done := true; Some (None, nfa.transition_func node None)) in
    Stream.from f

  let visit (nfa : ('S, 'A) t) (st : 'S) =
    let visited_nodes = Hashset.create !init_size in 
    let to_visit = Queue.create () in 
    Queue.add st to_visit;
    (* initialize pointers with dummy *)
    let nfa_src_outgoing_stream_ptr = ref (Stream.from (fun _ -> None)) in
    let src_ptr = ref st in
    (* return a stream that streams while to_visit isn't empty *)
    let rec f i =
      match Stream.peek !nfa_src_outgoing_stream_ptr with
      | None ->
        (* get next node when finished with previous node *)
        if Queue.is_empty to_visit then None else
        let src = Queue.pop to_visit in
        if Hashset.mem visited_nodes src then f i
        else begin
          (* new node *)
          Hashset.add visited_nodes src;
          nfa_src_outgoing_stream_ptr := get_outgoing nfa src;
          src_ptr := src;
          f i
        end
      | Some (char, dsts) -> begin
        Stream.junk !nfa_src_outgoing_stream_ptr;
        (* stream outgoing edges of current node *)
        let dsts_iter_func dst =
          if not (Hashset.mem visited_nodes dst)
          then Queue.add dst to_visit in 
        List.iter dsts_iter_func dsts;
        Some (!src_ptr, char, dsts)
      end in
    Stream.from f

  let edges (nfa : ('S, 'A) t) =
    visit nfa nfa.start_state

  let iter_outgoing (nfa : ('S, 'A) t) =
    let last_src_opt_ptr = ref None in 
    let outs_ptr = ref [] in 
    let nfa_edges_stream = edges nfa in
    let rec f i =
      match Stream.peek nfa_edges_stream with
      | Some (src, char, dsts) -> begin
        Stream.junk nfa_edges_stream;
        let last_src = match !last_src_opt_ptr with
          | None -> (last_src_opt_ptr := Some src; src) (* only on the first iteration *)
          | Some last_src -> last_src in 
        (* all iterations *)
        if src <> last_src
        then begin 
          last_src_opt_ptr := Some src;
          let outs = !outs_ptr in
          outs_ptr := [(char, dsts)];
          Some (last_src, outs)
        end else begin
          outs_ptr := (char, dsts) :: !outs_ptr; 
          f i (* loop *)
        end
      end
      | None -> begin
        (* edges stream drains, so about to end the loop *)
        match !last_src_opt_ptr with
        | None -> None
        | Some last_src -> begin
          last_src_opt_ptr := None;
          Some (last_src, !outs_ptr) 
        end
      end in
    Stream.from f

  let get_incoming (nfa : ('S, 'A) t) (node : 'S) =
    let nfa_edges_stream = edges nfa in
    let rec f i =
      match Stream.peek nfa_edges_stream with
      | None -> None
      | Some (src, _, dsts) -> 
        if List.exists (fun dst -> dst = node) dsts then Some src else f i in
    Stream.from f

  let get_reachable (nfa : ('S, 'A) t) (st : 'S) =
    (* Returns all reachable states. *)
    let last_src_opt_ptr = ref None in
    let nfa_visit_st_stream = visit nfa st in
    let rec f i =
      match Stream.peek nfa_visit_st_stream with
      | None -> None
      | Some (src, _, _) -> begin
        Stream.junk nfa_visit_st_stream;
        let src_opt = Some src in
        if src_opt <> !last_src_opt_ptr 
        then begin
          last_src_opt_ptr := src_opt;
          src_opt
        end else f i
      end in
    Stream.from f

  let states (nfa : ('S, 'A) t) =
    (* Returns all reachable states. *)
    get_reachable nfa nfa.start_state

  let end_states (nfa : ('S, 'A) t) =
    stream_filter nfa.accepted_states (states nfa)

  (* ======================================================================= *)
  let transition_table_of_hashtbl (tsx : ('S * 'A option, 'S list) Hashtbl.t) =
    (fun old_st i -> hashtbl_get tsx (old_st, i) [])

  let transition_table (table : (('S * 'A option) * 'S list) list) =
    let tsx = Hashtbl.create !init_size in
    List.iter (fun (k, v) -> Hashtbl.replace tsx k v) table;
    transition_table_of_hashtbl tsx

  let flatten (nfa : ('S, 'A) t) =
    (*  Flatten returns a new NFA that caches all of its transitions,
        essentially improving time but reducing space. *)
    let state_to_id = Hashtbl.create !init_size in
    let get_state_id st =
      match Hashtbl.find_opt state_to_id st with
      | Some st_id -> st_id
      | None ->
        let st_id = Hashtbl.length state_to_id in 
        Hashtbl.replace state_to_id st st_id; 
        st_id in
    let transitions = Hashtbl.create !init_size in
    let accepted_states_ptr = ref [] in 
    let nfa_iter_outgoing_iter_func (src, out) =
      let src_id = get_state_id src in 
      (if nfa.accepted_states src 
        then accepted_states_ptr := src_id :: !accepted_states_ptr);
      let out_iter_func (char, dsts) =
        Hashtbl.replace transitions (src_id, char) (List.rev_map get_state_id dsts) in 
      List.iter out_iter_func out in 
    Stream.iter nfa_iter_outgoing_iter_func (iter_outgoing nfa);
    create 
      nfa.alphabet 
      (transition_table_of_hashtbl transitions) 
      (Hashtbl.find state_to_id nfa.start_state) 
      (Accepted_List !accepted_states_ptr)

  let as_graph (nfa : ('S, 'A) t) =
    let visited_nodes = Hashset.create !init_size in
    let edges = Hashtbl.create !init_size in
    let nfa_iter_outgoing_iter_func (src, out) =
      Hashset.add visited_nodes src;
      let out_iter_func (char, dsts) =
        let dsts_iter_func dst = 
          let src_dst = (src, dst) in
          let chars = match Hashtbl.find_opt edges src_dst with
            | Some chars -> chars
            | None -> begin 
              let chars = Hashset.create !init_size in
              Hashtbl.replace edges src_dst chars;
              chars
            end in
          Hashset.add chars char in
        List.iter dsts_iter_func dsts in
      List.iter out_iter_func out in
    Stream.iter nfa_iter_outgoing_iter_func (iter_outgoing nfa);
    (visited_nodes, edges)

  let get_edges (nfa : ('S, 'A) t) =
    snd (as_graph nfa)

  let get_derivations (nfa : ('S, 'A) t) (seq : 'A option list) =
    (*  dedicated multi_transition and epsilon for get_derivations
        because Hashtbl.Make doesn't support user-defined types,
        thus can't use a user-defined hash function for derivation class,
        thus, use Hashtbl's keys for hashing, and values are derivations *)
    let multi_transition transition_func states char =
      let new_states = Hashtbl.create !init_size in 
      let states_iter_func _ drv_st =
        List.iter (fun st -> Hashtbl.replace new_states st#state st) (transition_func drv_st char) in 
      Hashtbl.iter states_iter_func states;
      new_states in
    let epsilon transition_func states =
      let states = Hashtbl.copy states in
      let rec loop () =
        let count = Hashtbl.length states in 
        Hashtbl.iter 
          (fun self_st drv_st -> Hashtbl.replace states self_st drv_st) 
          (multi_transition transition_func states None);
        if count = (Hashtbl.length states) then states else loop () in 
      loop () in
    (*  Returns a sequence of all paths between initial and accepting states.
        The sequence is empty when the input is rejected. Two derivations are
        deemed equivalent if they share the same accepting state. The sequence
        returns only *distinct* derivations. An implication of this is that if
        there are two derivations that reach the same target state, the shortest
        is picked. *)
    let tsx st char =
      List.rev_map st#link (nfa.transition_func st#state char) in
    let states_ptr = ref (Hashtbl.create 1) in
    Hashtbl.replace !states_ptr nfa.start_state (derivation nfa.start_state);
    let seq_iter_func i =
      if Hashtbl.length !states_ptr = 0
      (* Not accepted, break loop and return [] *)
      then true
      (* Get the transitions and then perform epsilon transitions *)
      else begin
        states_ptr := multi_transition tsx (epsilon tsx !states_ptr) i;
        false
      end in
    if List.exists seq_iter_func seq then []
    else begin
      let st_list = ref [] in
      Hashtbl.iter (fun _ st -> st_list := st :: !st_list) !states_ptr;
      List.rev_map (fun drv -> drv#to_list) !st_list
    end

  let remove_epsilon_transitions (nfa : ('S, 'A) t) =
    (* Returns an equivalent NFA that does not have epsilon-transitions *)
    let transition_func src char =
      if char = None then [] else
      (*  There are outgoing edges with epsilon transitions. Calculate the
          epsilon-closure and perform a transition on each state *)
      let dsts = self_multi_transition nfa (self_epsilon nfa (Hashset.of_list [src])) char in
      List.sort compare (Hashset.to_list dsts) in
    let accepted_states st =
      if nfa.accepted_states st then true 
      else Hashset.exists 
        (fun k -> nfa.accepted_states k) 
        (self_epsilon nfa (Hashset.of_list [st])) in
    create nfa.alphabet transition_func nfa.start_state (Accepted_Function accepted_states)

  let get_reachable_sinks (nfa : ('S, 'A) t) (st : 'S) =
    (*  Returns the list of reachable sink nodes, which always includes itself.
        When the node is not sink return None. *)
    if nfa.accepted_states st then None else
    let nfa_get_reachable_st_stream = get_reachable nfa st in
    let rec loop sinks =
      match Stream.peek nfa_get_reachable_st_stream with
      | None -> Some sinks
      | Some dst -> begin
        Stream.junk nfa_get_reachable_st_stream;
        if nfa.accepted_states dst then None else loop (dst :: sinks)
      end in
    loop []

  (* ======================================================================= *)
  let accepts (nfa : ('S, 'A) t) (inputs : 'A list) =
    (* Perform epsilon transitions of the state *)
    let states_ptr = ref (self_epsilon nfa (Hashset.of_list [nfa.start_state])) in 
    let inputs_iter_func i =
      if (Hashset.cardinal !states_ptr) = 0 
      (* Not accepted, break iteration and return false *)
      then false 
      (* Get the transitions and then perform epsilon transitions *)
      else begin
        states_ptr := self_epsilon nfa (self_multi_transition nfa !states_ptr (Some i)); 
        true (* continue iteration *)
      end in
    if List.for_all inputs_iter_func inputs 
    then (Hashset.cardinal (Hashset.filter nfa.accepted_states !states_ptr)) > 0
    else false

  let equal (nfa1 : ('S1, 'A) t) (nfa2 : ('S2, 'A) t) =
    (*  cannot compare NFAs with different state or alphabet types,
        do so will raise an exception *)
    (* Create a homo-morphism on states *)
    let n1 = flatten nfa1 in
    let n2 = flatten nfa2 in
    n1.start_state = n2.start_state &&
    (Hashset.equal (Hashset.of_stream (end_states n1)) (Hashset.of_stream (end_states n2))) &&
    equal_graph (as_graph n1) (as_graph n2)

  let is_sink (nfa : ('S, 'A) t) (st : 'S) =
    (* Tests if a state is a sink node (only contains self loops) *)
    get_reachable_sinks nfa st <> None

  let remove_sink_states (nfa : ('S, 'A) t) =
    (* Removes edges that directly connect to a sink node. *)
    let tsx = nfa.transition_func in
    let sink_cache = Hashtbl.create !init_size in
    let is_sink (st : 'S) =
      match Hashtbl.find_opt sink_cache st with
      | Some snk -> snk
      | None -> begin
        match get_reachable_sinks nfa st with
        | None -> (Hashtbl.replace sink_cache st false; false)
        | Some nodes -> (List.iter (fun n -> Hashtbl.replace sink_cache n true) nodes; true)
      end in
    let transition_func (src : 'S) (char : 'A option) =
      if is_sink src then [] 
      else List.filter (fun x -> not (is_sink x)) (tsx src char) in
    create nfa.alphabet transition_func nfa.start_state (Accepted_Function nfa.accepted_states)

  let size (nfa : ('S, 'A) t) =
    (* Returns how many states the NFA contains *)
    let nfa_states_stream = states nfa in
    let rec junk_all () =
      match Stream.peek nfa_states_stream with
      | None -> ()
      | Some _ -> (Stream.junk nfa_states_stream; junk_all ()) in
    junk_all ();
    Stream.count nfa_states_stream

  let is_empty (nfa : ('S, 'A) t) =
    let nfa_end_states_stream = end_states nfa in
    match Stream.peek nfa_end_states_stream with
    | None -> true
    | Some _ -> false

  (* ======================================================================= *)
  let to_string (nfa : ('S, 'A) t) 
                (str_of_st : 'S -> string) 
                (str_of_char : 'A -> string) =
    (* requires string conversion functions for states and alphabet to be given *)
    let open Buffer in
    let open Printf in
    let data = create !init_size in
    let transitions = snd (as_graph nfa) in
    (* start_state *)
    add_string data (sprintf "NFA({start_state: %s, accepted_states: [" (str_of_st nfa.start_state));
    (* accepted_states *)
    let is_fst_st_iter = ref true in
    let end_states_iter_func st =
      (if !is_fst_st_iter then is_fst_st_iter := false else add_string data ", ");
      add_string data (str_of_st st) in
    Stream.iter end_states_iter_func (end_states nfa);
    add_string data "], transitions: {";
    (* transitions *)
    let is_fst_tran_iter = ref true in
    let transitions_iter_func (src, dst) chars =
      (if !is_fst_tran_iter then is_fst_tran_iter := false else add_string data ", ");
      add_string data (sprintf "(%s, %s): {" (str_of_st src) (str_of_st dst));
      let is_fst_char_iter = ref true in
      let chars_iter_func char_opt =
        (if !is_fst_char_iter then is_fst_char_iter := false else add_string data ", ");
        add_string data (match char_opt with None -> "ε" | Some char -> str_of_char char) in
      Hashset.iter chars_iter_func chars;
      add_string data "}" in
    Hashtbl.iter transitions_iter_func transitions;
    add_string data "}})";
    contents data

  let as_dict ?(is_flatten=true) (nfa : (int, 'A) t) =
    (*  type of states must be int, 
        if want state type be any type, 
        need to write a function w/o flatten option *)
    (*  Converts this NFA into a nfa_dict type.
        The alphabet must be serializable. *)
    let nfa = if is_flatten then flatten nfa else nfa in
    let edges = ref [] in
    let dfa_as_graph_edges = snd (as_graph nfa) in
    let dfa_as_graph_edges_iter_func (src, dst) chars =
      let chars_iter_func char =
        edges := { src = src; char = char; dst = dst } :: !edges in
      Hashset.iter chars_iter_func chars in
    Hashtbl.iter dfa_as_graph_edges_iter_func dfa_as_graph_edges;
    { start_state = nfa.start_state; 
      accepted_states = (rev_list_of_stream (end_states nfa)); 
      edges = !edges }

  let from_dict ?alphabet (nfa : ('S, 'A) dict) =
    (* Creates a NFA from a dictionary-based DFA (see `as_dict`). *)
    let alphabet = match alphabet with
      | Some alp -> alp
      | None -> begin (* duplicates will be removed in the create function *)
        let alp = Hashset.create !init_size in
        List.iter (fun edge -> 
          match edge.char with
          | Some char -> Hashset.add alp char
          | None -> ()
        ) nfa.edges;
        alp
      end in
    let tbl = Hashtbl.create !init_size in
    let dfa_edges_iter_func edge =
      let src_char = (edge.src, edge.char) in
      match Hashtbl.find_opt tbl src_char with
      | None -> Hashtbl.replace tbl src_char [edge.dst]
      | Some outs -> Hashtbl.replace tbl src_char (edge.dst :: outs) in
    List.iter dfa_edges_iter_func nfa.edges;
    create alphabet (transition_table_of_hashtbl tbl) nfa.start_state (Accepted_List nfa.accepted_states)

  let transition_edges_split (edges : ('S * 'A option * 'S) list) =
    let tsx = Hashtbl.create !init_size in
    let edges_iter_func (src, char, dst) =
      let src_char = (src, char) in
      match Hashtbl.find_opt tsx src_char with
      | None -> Hashtbl.replace tsx src_char [dst]
      | Some elems -> Hashtbl.replace tsx src_char (dst :: elems) in
    List.iter edges_iter_func edges;
    transition_table_of_hashtbl tsx

  let transition_edges (edges : ('S * 'A option list * 'S) list) =
    let tsx = Hashtbl.create !init_size in
    let edges_iter_func (src, chars, dst) =
      let chars_iter_func char =
        let src_char = (src, char) in
        match Hashtbl.find_opt tsx src_char with
        | None -> Hashtbl.replace tsx src_char [dst]
        | Some elems -> Hashtbl.replace tsx src_char (dst :: elems) in
      List.iter chars_iter_func chars in
    List.iter edges_iter_func edges;
    transition_table_of_hashtbl tsx

  let from_transition_edges ?alphabet
                            (edges : ('S * 'A option list * 'S) list)
                            (start_state : 'S)
                            (accepted_states : 'S accepted_type) =
    let alphabet = match alphabet with
      | Some alp -> alp 
      | None -> begin
        let alp = Hashset.create !init_size in
        let edges_iter_func (_, chars, _) =
          List.iter (fun c_opt -> 
            match c_opt with 
            | Some c -> Hashset.add alp c
            | None -> ()
          ) chars in
        List.iter edges_iter_func edges;
        alp
      end in
    create alphabet (transition_edges edges) start_state accepted_states

  let from_transition_table ?alphabet
                            (table : ('S * 'A option, 'S list) Hashtbl.t)
                            (start_state : 'S)
                            (accepted_states : 'S accepted_type) =
    let alphabet = match alphabet with
      | Some alp -> alp 
      | None -> begin
        let alp = Hashset.create !init_size in
        Hashtbl.iter 
          (fun (_, c_opt) _ -> 
            match c_opt with 
            | Some c -> Hashset.add alp c
            | None -> ()
          ) table;
        alp
      end in
    create alphabet (transition_table_of_hashtbl table) start_state accepted_states

  (* Usual NFA constructors ================================================ *)
  let make_nil (alphabet : 'A Hashset.t) =
    let q1 = 0 in
    create alphabet (fun q a_opt -> []) q1 (Accepted_Function (fun x -> x = q1))

  let make_char (alphabet : 'A Hashset.t) (char : 'A) =
    assert (Hashset.mem alphabet char); (* char not in alphabet *)
    let q1 = 0 and q2 = 1 in
    let dst_q2 = [q2] in
    let transition q a_opt =
      match a_opt with
      | Some a -> if a = char && q = q1 then dst_q2 else []
      | None -> [] in
    create alphabet transition q1 (Accepted_List dst_q2)

  let make_any_from (chars : 'A Hashset.t) (alphabet : 'A Hashset.t) =
    assert (Hashset.subset chars alphabet); (* chars not a subset of alphabet *)
    let q1 = 0 and q2 = 1 in
    let dst_q2 = [q2] in
    let transition q a_opt =
      match a_opt with
      | Some a -> if (Hashset.mem chars a) && q = q1 then dst_q2 else []
      | None -> [] in
    create alphabet transition q1 (Accepted_List dst_q2)

  let make_any (alphabet : 'A Hashset.t) =
    let q1 = 0 and q2 = 1 in
    let dst_q2 = [q2] in
    let transition q a_opt =
      if q = q1 then dst_q2 else [] in
    create alphabet transition q1 (Accepted_List dst_q2)

  let make_all (alphabet : 'A Hashset.t) =
    let q1 = 0 in
    let dst_q1 = [q1] in
    let transition q a_opt = dst_q1 in
    create alphabet transition q1 (Accepted_List dst_q1)

  let make_contains (alphabet : 'A Hashset.t) (char : 'A) =
    assert (Hashset.mem alphabet char); (* char not in alphabet *)
    let q1 = 0 and q2 = 1 in
    let dst_q2 = [q2] in
    let transition q a_opt =
      match a_opt with
      | Some a -> if a = char && q = q1 then dst_q2 else [q]
      | None -> [q] in
    create alphabet transition q1 (Accepted_List dst_q2)

  let make_void (alphabet : 'A Hashset.t) =
    let q1 = 0 in
    create alphabet (fun q a_opt -> []) q1 (Accepted_Function (fun x -> false))
end

let remove_back_edges (edges : ('S * 'A option * 'S) Hashset.t) (states : 'S Hashset.t) =
  Hashset.filter (fun (_, _, x) -> not (Hashset.mem states x)) edges

let edges_to_map (edges : ('S * 'A option * 'S) Hashset.t) =
  let result = Hashtbl.create !init_size in
  let edges_iter_func (src, char, dst) =
    let key = (src, char) in
    let data = match Hashtbl.find_opt result key with
      | Some data -> data
      | None -> begin
        let data = Hashset.create !init_size in
        Hashtbl.replace result key data;
        data
      end in
    Hashset.add data dst in
  Hashset.iter edges_iter_func edges;
  result

type ('S, 'A) step = {
  source : ('S * int);
  edge : 'A option;
  target : ('S * int) }

let reduction_tree (levels : ('S * 'A option * 'S) Hashset.t list) (accepted_states : 'S Hashset.t) =
  let result = ref [] in
  let nodes = ref (Hashtbl.create !init_size) in
  let levels_iter_func lvl =
    let lvl_map = edges_to_map lvl in
    let edges = ref [] in
    let next_nodes = Hashtbl.copy !nodes in
    let lvl_map_iter_func _ outs =
      let outs_iter_func st =
        Hashtbl.replace next_nodes st ((hashtbl_get !nodes st 0) + 1) in
      Hashset.iter outs_iter_func outs in
    Hashtbl.iter lvl_map_iter_func lvl_map;
    let lvl_map_iter_func (src_st, char) outs =
      let outs_iter_func dst_st =
        let prev_nodes = if char <> None then !nodes else next_nodes in
        edges := {
          source = (src_st, hashtbl_get prev_nodes src_st 0);
          edge = char;
          target = (dst_st, hashtbl_get next_nodes dst_st 0);
        } :: !edges in
      Hashset.iter outs_iter_func outs in
    Hashtbl.iter lvl_map_iter_func lvl_map;
    nodes := next_nodes;
    result := !edges :: !result in
  List.iter levels_iter_func levels;
  let accepted = ref [] in
  let accepted_states_iter_func st =
    accepted := (st, hashtbl_get !nodes st 0) :: !accepted in
  Hashset.iter accepted_states_iter_func accepted_states;
  (!result, !accepted)

module DerivationGraph = struct
  type ('S, 'A) t = { 
    alphabet : 'A Hashset.t;
    transition_func : 'S -> 'A option -> 'S list;
    start_state : 'S;
    accepted_states : 'S -> bool }

  let create (alphabet : 'A Hashset.t) 
             (transition_func : 'S -> 'A option -> 'S list) 
             (start_state : 'S) 
             (accepted_states : 'S -> bool) = 
    { alphabet = alphabet;
      transition_func = transition_func;
      start_state = start_state;
      accepted_states = accepted_states }

  let self_multi_transition (nfa : ('S, 'A) t) (states : 'S Hashset.t) (char : 'A option) =
    multi_transition nfa.transition_func states char

  let self_epsilon (nfa : ('S, 'A) t) (states : 'S Hashset.t) = 
    epsilon nfa.transition_func states

  let multi_transition_edges (nfa : ('S, 'A) t) (states : 'S Hashset.t) (char : 'A option) =
    (* Returns the outgoing edges from a set of states. *)
    let result = Hashset.create !init_size in
    let states_iter_func src =
      let dsts_iter_func dst =
        Hashset.add result (src, char, dst) in
      List.iter dsts_iter_func (nfa.transition_func src char) in
    Hashset.iter states_iter_func states;
    result
    
  let epsilon_edges_unfold_hops (nfa : ('S, 'A) t) (states : 'S Hashset.t) = 
    let row = ref (remove_back_edges (multi_transition_edges nfa states None) states) in
    let all_edges = Hashset.copy !row in
    let curr_states = ref (Hashset.copy states) in
    let next_states = ref (Hashset.union !curr_states (self_multi_transition nfa !curr_states None)) in
    let rec f i =
      if (Hashset.cardinal !next_states) <> (Hashset.cardinal !curr_states) 
      then begin
        let result = Some !row in
        curr_states := !next_states;
        let mut_row = Hashset.create !init_size in
        let edges_iter_func edge =
          (* Make sure that the edge is new and is not a back edge *)
          let (_, _, dst) = edge in
          if (not (Hashset.mem all_edges edge)) && (not (Hashset.mem !curr_states dst))
          then (Hashset.add all_edges edge; Hashset.add mut_row edge) in
        Hashset.iter edges_iter_func (multi_transition_edges nfa !curr_states None);
        row := mut_row;
        next_states := Hashset.union !curr_states (self_multi_transition nfa !curr_states None);
        result
      end else None in
    Stream.from f

  let epsilon_edges_fold_hops (nfa : ('S, 'A) t) (states : 'S Hashset.t) =
    let result = Hashset.create !init_size in
    Stream.iter (fun row -> Hashset.update result row) (epsilon_edges_unfold_hops nfa states);
    let is_done = ref false in
    let f i =
      if !is_done then None 
      else if (Hashset.cardinal result) > 0 
      then (is_done := true; Some result )
      else None in
    Stream.from f

  let epsilon_edges ?(fold_hops=false) (nfa : ('S, 'A) t) (states : 'S Hashset.t) =
    if fold_hops 
    then epsilon_edges_fold_hops nfa states
    else epsilon_edges_unfold_hops nfa states

  let accepts_edges ?(fold_epsilon_hops=false) (nfa : ('S, 'A) t) (inputs : 'A list) =
    let result_opt = ref None in
    let states = ref (self_epsilon nfa (Hashset.of_list [nfa.start_state])) in
    let frontier = ref [] in
    Stream.iter 
      (fun lvl -> frontier := lvl :: !frontier) 
      (epsilon_edges ~fold_hops:fold_epsilon_hops nfa (Hashset.of_list [nfa.start_state]));
    let inputs_iter_func i =
      if Hashset.is_empty !states
      then (result_opt := Some (!frontier, Hashset.empty ()); true)
      else begin
        frontier := (multi_transition_edges nfa !states (Some i)) :: !frontier;
        states := self_multi_transition nfa !states (Some i);
        Stream.iter 
          (fun lvl -> frontier := lvl :: !frontier) 
          (epsilon_edges ~fold_hops:fold_epsilon_hops nfa !states);
        states := epsilon nfa.transition_func !states;
        false
      end in
    ignore (List.exists inputs_iter_func inputs);
    match !result_opt with
    | Some result -> result
    | None -> (!frontier, Hashset.filter nfa.accepted_states !states)

  let accepts_derivation ?(fold_epsilon_hops=false) (nfa : ('S, 'A) t) (word : 'A list) =
    let (lvls, final) = accepts_edges ~fold_epsilon_hops:fold_epsilon_hops nfa word in
    reduction_tree lvls final
end

let nfa_derivation_graph (nfa : ('S, 'A) NFA.t) (word : 'A list) =
  DerivationGraph.accepts_derivation 
    (DerivationGraph.create
      nfa.alphabet
      nfa.transition_func
      nfa.start_state
      nfa.accepted_states) 
    word

let nfa_to_dfa (nfa : ('S, 'A) NFA.t) =
  let transition (q : 'S list) (c : 'A) =
    Hashset.to_list (epsilon nfa.transition_func (multi_transition nfa.transition_func (Hashset.of_list q) (Some c))) in
  let accept_state (qs : 'S list) =
    List.exists nfa.accepted_states qs in
  DFA.create
    nfa.alphabet
    transition
    (Hashset.to_list (epsilon nfa.transition_func (Hashset.of_list [nfa.start_state])))
    (Accepted_Function accept_state)

let dfa_to_nfa (dfa : ('S, 'A) DFA.t) =
  let transition_func (q : 'S) (a_opt : 'A option) =
    match a_opt with
    | Some a -> [dfa.transition_func q a]
    | None -> [] in
  NFA.create
    dfa.alphabet
    transition_func
    dfa.start_state
    (Accepted_Function dfa.accepted_states)

(* ######################################################################### *)
(* REGEX *)

module REGEX = struct
  type t =
    | Nil
    | Void
    | Concat of t * t
    | Union of t * t
    | Star of t
    | Char of string

  let is_primitive = function
    | Nil -> true
    | Void -> true
    | Char _ -> true
    | _ -> false

  let concat left right = 
    match (left, right) with
    | (Nil, _) -> right
    | (_, Nil) -> left
    | (_, Void) | (Void, _) -> Void
    | (_, _) -> Concat (left, right)
    
  let concat_from_list args =
    let rec loop result = function
      | elem :: rest_args -> loop (concat result elem) rest_args
      | [] -> result in
    loop Nil args

  let union left right = 
    match (left, right) with
    | (Void, _) -> right
    | (_, Void) -> left
    | (Nil, Star _) -> right
    | (Star _, Nil) -> left
    | (_, _) -> Union (left, right)

  let union_flatten n =
    let result = Hashset.create !init_size in
    let to_visit = Queue.create () in
    Queue.add to_visit n;
    let rec loop () =
      if not (Hashset.is_empty result) 
      then begin
        let r = Queue.pop to_visit in
        (match r with
        | Union (left, right) -> begin
          Queue.add left to_visit;
          Queue.add right to_visit
        end
        | Void -> ()
        | _ -> Hashset.add result r);
        loop ()
      end in
    loop ();
    result

  let union_from_list iterable =
    let rec loop result = function
      | x :: l -> begin
        match result with
        | Void -> loop x l
        | _ -> loop (Union (x, result)) l
      end
      | [] -> result in
    loop Void iterable

  let shuffle c1 c2 =
    Union ((concat c1 c2), (concat c1 c2))

  let star child = 
    match child with
    | Nil | Void -> Nil
    | Star _ -> child
    | _ -> Star child

  class regex_to_str (char_str : string -> string)
                    (void_str : string) 
                    (nil_str : string) 
                    (app_str : string -> string -> string) 
                    (union_str : string -> string -> string) 
                    (star_str : string -> string) =
    object (self)
      method on_void = void_str
      method on_nil = nil_str
      method paren r text = 
        if is_primitive r then text else "(" ^ text ^ ")"
      method on_concat r_left r_right left right = 
        app_str (self#paren r_left left) (self#paren r_right right)
      method on_union r_left r_right left right =
        let p_left = self#paren r_left left in
        let p_right = self#paren r_right right in
        match (r_left, r_right) with
        | (Union _, Union _) -> union_str left right
        | (Union _, _) -> union_str left p_right
        | (_, Union _) -> union_str p_left right
        | (_, _) -> union_str p_left p_right
      method on_star r_child child = star_str (self#paren r_child child)
      method on_char r_char = char_str r_char
    end

  class subst_handler =
    (*  A regular expression handler that returns a regular expression.
        It is smart in that it does not re-create parts of the tree that remain
        unchanged. *)
    object (self)
      method on_nil = Nil
      method on_void = Void
      method on_char (char : t) = char
      method on_concat r_left r_right left right =
        if r_left = left && r_right = right then Concat (r_left, r_right) else Concat (left, right)
      method on_union r_left r_right left right =
        if r_left = left && r_right = right then Union (r_left, r_right) else Union (left, right)
      method on_star r_child child =
        if r_child = child then Star r_child else Star child
    end

  let rec fold reg handler = 
    match reg with
    | Union (r_left, r_right) ->
      let left = fold r_left handler in
      let right = fold r_right handler in
      handler#on_union r_left r_right left right
    | Concat (r_left, r_right) ->
      let left = fold r_left handler in
      let right = fold r_right handler in
      handler#on_concat r_left r_right left right
    | Star r_child ->
      let child = fold r_child handler in
      handler#on_star r_child child
    | Void -> 
      handler#on_void
    | Nil -> 
      handler#on_nil
    | Char r_char -> 
      handler#on_char r_char

  let rec exists reg pred = 
    match reg with
    | Union (r_left, r_right) ->
      exists r_left pred || 
      exists r_right pred || 
      pred reg
    | Concat (r_left, r_right) ->
      exists r_left pred || 
      exists r_right pred || 
      pred reg
    | Star r_child ->
      exists r_child pred || pred reg
    | Void -> pred reg
    | Nil -> pred reg
    | _ -> failwith "ValueError(reg)"

  let to_string ?(char_str=(fun x -> x)) ?(void_str="{}") ?(nil_str="[]") reg =
    let app_str = (fun x y -> x ^ "·" ^ y) in
    let union_str = (fun x y -> x ^ "+" ^ y) in
    let star_str = (fun x -> x ^ "*") in
    fold reg (new regex_to_str char_str void_str nil_str app_str union_str star_str)

  let get_alphabet r =
    let result = Hashset.create !init_size in
    let to_process = Queue.create () in
    Queue.add r to_process;
    let rec loop () =
      if not (Queue.is_empty to_process)
      then begin
        let elem = Queue.pop to_process in
        (match elem with
        | Char char -> Hashset.add result char
        | Void | Nil -> ()
        | Star child -> Queue.add child to_process
        | Union (left, right) | Concat (left, right) -> begin
          Queue.add left to_process;
          Queue.add right to_process
        end);
        loop ()
      end in
    loop ();
    result
end

(* ========================================================================= *)
type 'S wrap_state =
  | Start
  | Mid of 'S
  | End

let wrap_state_to_tuple = function
 | Start -> (0, None)
 | Mid state -> (1, state)
 | End -> (2, None)

let wrap (nfa : ('S, 'A) NFA.t) =
  let start = Start in
  let ed = End in
  let init = Mid nfa.start_state in
  let init_s = [init] in
  let tsx st a = match st with
    | Start -> if a = None then init_s else []
    | End -> []
    | Mid state ->
      let rest = List.rev_map (fun x -> Mid x) (nfa.transition_func state a) in
      if nfa.accepted_states state && a = None
      then ed :: rest
      else rest in
  NFA.create
    nfa.alphabet
    tsx
    start
    (Accepted_List [ed])

module GNFA = struct
  type ('S, 'A) t = {
    states : 'S Hashset.t;
    alphabet : 'A Hashset.t;
    transitions : ('S * 'S, REGEX.t) Hashtbl.t;
    start_state : 'S;
    end_state : 'S }

  let create (states : 'S Hashset.t)
             (alphabet : 'A Hashset.t)
             (transitions : ('S * 'S, REGEX.t) Hashtbl.t)
             (start_state : 'S)
             (end_state : 'S) =
    { states = states;
      alphabet = alphabet;
      transitions = transitions;
      start_state = start_state;
      end_state = end_state }

  let get_outgoing = NFA.get_outgoing

  let get_incoming = NFA.get_incoming

  let as_graph (gnfa : ('S, 'A) t) = (gnfa.states, gnfa.transitions)

  let accepted_states (gnfa : ('S, 'A) t) (st : 'S) = st = gnfa.end_state

  let step ?node ?(sort=false) (gnfa : ('S, 'A) t) =
    if Hashset.cardinal gnfa.states <= 2 then failwith "ValueError()" else
    let states = Hashset.copy gnfa.states in
    (* Pick a state that is not start/end *)
    let node = match node with
      | None -> begin
        let n = ref gnfa.start_state in
        let states_iter_func elem =
          if elem <> gnfa.start_state && elem <> gnfa.end_state 
          then begin
            n := elem;
            true (* break loop *)
          end else false in
        ignore (Hashset.exists states_iter_func states);
        !n
      end
      | Some n -> begin
        if n = gnfa.start_state
        then failwith "Node to remove {node} cannot be the start state."
        else if n = gnfa.end_state
        then failwith "Node to remove {node} cannot be the end state."
        else n
      end in
    Hashset.remove states node;
    let tsx = Hashtbl.create !init_size in
    let states_list = 
      if sort 
      then List.sort compare (Hashset.to_list states)
      else Hashset.to_list states in
    let si = states_list in
    let sj = states_list in
    let si_iter_func qi =
      let sj_iter_func qj =
        let r1 = hashtbl_get gnfa.transitions (qi, node) Void in
        let r2 = hashtbl_get gnfa.transitions (node, node) Void in
        let r3 = hashtbl_get gnfa.transitions (node, qj) Void in
        let r4 = hashtbl_get gnfa.transitions (qi, qj) Void in
        let result = REGEX.union (REGEX.concat r1 (REGEX.concat (REGEX.star r2) r3)) r4 in
        if result <> Void
        then Hashtbl.replace tsx (qi, qj) result in
      List.iter sj_iter_func sj in
    List.iter si_iter_func si;
    let gnfa = create
      states
      gnfa.alphabet
      tsx
      gnfa.start_state
      gnfa.end_state in
    (node, gnfa)

  let reduce (gnfa : ('S, 'A) t) =
    let gnfa_ptr = ref gnfa in
    let f i =
      if Hashset.cardinal (!gnfa_ptr.states) > 2
      then begin
        let (state, gnfa) = step !gnfa_ptr in
        gnfa_ptr := gnfa;
        Some (state, gnfa)
      end else None in
    Stream.from f

  let to_regex (gnfa : ('S, 'A) t) =
    let m_ptr = ref [] in
    Stream.iter (fun (_, g) -> m_ptr := g :: !m_ptr) (reduce gnfa);
    let last = List.hd !m_ptr in
    hashtbl_get last.transitions (gnfa.start_state, gnfa.end_state) Nil

  let from_nfa (nfa : ('S, string) NFA.t) =
    let wrapped_nfa = wrap nfa in
    let start = wrapped_nfa.start_state in
    let ed = match Stream.peek (NFA.end_states wrapped_nfa) with
      | Some x -> x
      | None -> failwith "wrapped_nfa has no end states" in
    let edges = snd (NFA.as_graph wrapped_nfa) in
    let tsx = Hashtbl.create !init_size in
    let edges_iter_func (q1, q2) chars =
      let all_chars_ptr = ref [] in
      let chars_iter_func a =
        all_chars_ptr := REGEX.( 
          match a with
          | Some c -> Char c
          | None -> Nil
        ) :: !all_chars_ptr in
      Hashset.iter chars_iter_func chars;
      Hashtbl.replace tsx (q1, q2) (REGEX.union_from_list !all_chars_ptr) in
    Hashtbl.iter edges_iter_func edges;
    create
      (Hashset.of_stream (NFA.states wrapped_nfa))
      wrapped_nfa.alphabet
      tsx
      start
      ed
end

let rec regex_to_nfa ?alphabet r =
  let alphabet = match alphabet with (* Compute the alphabet automatically *)
    | Some alp -> alp
    | None -> REGEX.get_alphabet r in
  match r with
  | Void -> (* (int, char) Regular.NFA.t *)
    let nfa = NFA.make_void alphabet in
    NFA.flatten nfa
  | Nil -> (* (int, char) Regular.NFA.t *)
    let nfa = NFA.make_nil alphabet in
    NFA.flatten nfa
  | Char r_char -> (* (int, char) Regular.NFA.t *)
    let nfa = NFA.make_char alphabet r_char in
    NFA.flatten nfa
  | Concat (r_left, r_right) -> (* (('a, 'b) Regular.concat_state, 'c) Regular.NFA.t *)
    let left = regex_to_nfa ~alphabet r_left in
    let right = regex_to_nfa ~alphabet r_right in
    let nfa = NFA.concat left right in
    NFA.flatten nfa
  | Union (r_left, r_right) -> (* (int * int, 'a) Regular.NFA.t *)
    let left = regex_to_nfa ~alphabet r_left in
    let right = regex_to_nfa ~alphabet r_right in
    let nfa = NFA.union left right in
    NFA.flatten nfa
  | Star r_child -> (* ('a Regular.start_state, 'b) Regular.NFA.t *)
    let child = regex_to_nfa ~alphabet r_child in
    let nfa = NFA.star child in
    NFA.flatten nfa

let nfa_to_regex (nfa : ('S, string) NFA.t) =
  (* If there are no accepting states, then its language is the empty set. *)
  if NFA.is_empty nfa then REGEX.Void else
  let g = GNFA.from_nfa nfa in
  GNFA.to_regex g