type atom =
  | Nil
  | Void
  | All

type ('S, 'A) d =
  | D_Atom of atom
  | Dfa of ('S, 'A) Regular.DFA.t
  | D_Union of (('S, 'A) d * ('S, 'A) d)
  | D_Intersect of (('S, 'A) d * ('S, 'A) d)
  | Subtract of (('S, 'A) d * ('S, 'A) d)
  | Complement of ('S, 'A) d
  | Minimize of ('S, 'A) d
  | Nfa2dfa of ('S, 'A) n

and ('S, 'A) n = 
  | N_Atom of atom
  | Nfa of ('S, 'A) Regular.NFA.t
  | Shuffle of (('S, 'A) n * ('S, 'A) n)
  | N_Union of (('S, 'A) n * ('S, 'A) n)
  | Concat of (('S, 'A) n * ('S, 'A) n)
  | Star of ('S, 'A) n
  | Remove_sinks of ('S, 'A) n
  | Dfa2nfa of ('S, 'A) d

type ('S, 'A) b = 
  | Accepts of (('S, 'A) d * 'A list)
  | Is_empty of ('S, 'A) d
  | Is_sink of (('S, 'A) d * int)
  | Equal of (('S, 'A) d * ('S, 'A) d)
  | Equivalent of (('S, 'A) d * ('S, 'A) d)
  | Contains of (('S, 'A) d * ('S, 'A) d)
  | N_Accepts of (('S, 'A) n * 'A list)
  | N_Is_empty of ('S, 'A) n
  | N_Is_sink of (('S, 'A) n * int)
  | N_Equal of (('S, 'A) n * ('S, 'A) n)

type ('S, 'A) t =
  | Dfa_exp of ('S, 'A) d
  | Nfa_exp of ('S, 'A) n
  | Bool_exp of ('S, 'A) b

type ('S, 'A) result =
  | R_dfa of ('S, 'A) Regular.DFA.t
  | R_nfa of ('S, 'A) Regular.NFA.t
  | R_bool of bool

(* ========================================================================= *)
(* private functions - not meant for users to use *)

let get_alphabet wrapped_exp =
  let default_alphabet = Hashset.of_list ["a"] in
  let dfa_opt = ref None in
  let nfa_opt = ref None in
  let rec find_d = function
    | D_Atom _ -> false
    | Dfa dfa -> begin
      dfa_opt := Some dfa;
      true
    end
    | D_Union (left, right) -> (find_d left) || (find_d right)
    | D_Intersect (left, right) -> (find_d left) || (find_d right)
    | Subtract (left, right) -> (find_d left) || (find_d right)
    | Complement exp -> find_d exp
    | Minimize exp -> find_d exp
    | Nfa2dfa nfa_exp -> find_n nfa_exp
  and find_n = function
    | N_Atom _ -> false
    | Nfa nfa -> begin
      nfa_opt := Some nfa;
      true
    end
    | Shuffle (left, right) -> (find_n left) || (find_n right)
    | N_Union (left, right) -> (find_n left) || (find_n right)
    | Concat (left, right) -> (find_n left) || (find_n right)
    | Star exp -> find_n exp
    | Remove_sinks exp -> find_n exp
    | Dfa2nfa dfa_exp -> find_d dfa_exp
  and find_b = function
    | Accepts (exp, word) -> find_d exp
    | Is_empty exp -> find_d exp
    | Is_sink (exp, state) -> find_d exp
    | Equal (left, right) -> (find_d left) || (find_d right)
    | Equivalent (left, right) -> (find_d left) || (find_d right)
    | Contains (left, right) -> (find_d left) || (find_d right)
    | N_Accepts (exp, word) -> find_n exp
    | N_Is_empty exp -> find_n exp
    | N_Is_sink (exp, state) -> find_n exp
    | N_Equal (left, right) -> (find_n left) || (find_n right) in
  ignore begin
    match wrapped_exp with
    | Dfa_exp d -> find_d d
    | Nfa_exp n -> find_n n
    | Bool_exp b -> find_b b
  end;
  match !dfa_opt with
  | None -> begin
    match !nfa_opt with
    | None -> default_alphabet
    | Some nfa -> nfa.alphabet
  end
  | Some dfa -> dfa.alphabet

let equal_a (a:atom) (b: atom) : bool =
  match (a,b) with
  | Nil, Nil -> true
  | Void, Void -> true
  | All, All -> true
  | _, _ -> false

let rec equal_d a b =
  match (a, b) with
  | (D_Atom a, D_Atom b) -> equal_a a b
  | (D_Union (a, b), D_Union (c, d)) -> (equal_d a c) && (equal_d b d)
  | (D_Intersect (a, b), D_Intersect (c, d)) -> (equal_d a c ) && (equal_d b d)
  | (Subtract (a, b), Subtract (c, d)) -> (equal_d a c ) && (equal_d b d)
  | (Complement a, Complement b) -> equal_d a b
  | (Minimize a, Minimize b) -> equal_d a b
  | (Nfa2dfa a, Nfa2dfa b) -> equal_n a b
  | _ -> false
and equal_n a b =
  match (a, b) with
  | (N_Atom a, N_Atom b) -> equal_a a b
  | (Shuffle (a, b), Shuffle (c, d)) -> (equal_n a c) && (equal_n b d)
  | (N_Union (a, b), N_Union (c, d)) -> (equal_n a c) && (equal_n b d)
  | (Concat (a, b), Concat (c, d)) -> (equal_n a c) && (equal_n b d)
  | (Star a, Star b) -> equal_n a b
  | (Remove_sinks a, Remove_sinks b) -> equal_n a b
  | (Dfa2nfa a, Dfa2nfa b) -> equal_d a b
  | _ -> false

let equal_b a b =
  match (a, b) with
  | (Accepts (a, a_word), Accepts (b, b_word)) -> equal_d a b && a_word = b_word
  | (Is_empty a, Is_empty b) -> equal_d a b
  | (Is_sink (a, a_st), Is_sink (b, b_st)) -> equal_d a b && a_st = b_st
  | (Equal (a, b), Equal (c, d)) -> equal_d a c && equal_d b d
  | (Equivalent (a, b), Equivalent (c, d)) -> equal_d a c && equal_d b d
  | (Contains (a, b), Contains (c, d)) -> equal_d a c && equal_d b d
  | (N_Accepts (a, a_word), N_Accepts (b, b_word)) -> equal_n a b && a_word = b_word
  | (N_Is_empty a, N_Is_empty b) -> equal_n a b
  | (N_Is_sink (a, a_st), N_Is_sink (b, b_st)) -> equal_n a b && a_st = b_st
  | (N_Equal (a, b), N_Equal (c, d)) -> equal_n a c && equal_n b d
  | _ -> false

let rec equal wrapped_a wrapped_b =
  match (wrapped_a, wrapped_b) with
  | (Dfa_exp a, Dfa_exp b) -> equal_d a b
  | (Nfa_exp a, Nfa_exp b) -> equal_n a b
  | (Bool_exp a, Bool_exp b) -> equal_b a b
  | _ -> failwith "cannot compare different types"

let rec optimize wrapped_exp =
  (* minimized/sinks_removed flags is for removing Minimize/Remove_sinks recursively *)
  let minimized = ref false in
  let sinks_removed = ref false in
  let rec opt_d = function
    | D_Atom a -> D_Atom a
    | Dfa dfa -> Dfa dfa
    | D_Union (left, right) -> begin
      match (opt_d left, opt_d right) with
      | (a, b) when equal_d a b -> a
      | (D_Atom Void, a) | (a, D_Atom Void) -> a
      | (D_Atom All, _) | (_, D_Atom All) -> D_Atom All
      | (D_Intersect (a, b), D_Intersect (c, d)) when equal_d a c -> D_Intersect (a, D_Union (b, d))
      | (D_Intersect (a, b), D_Intersect (d, c)) when equal_d a c -> D_Intersect (a, D_Union (b, d))
      | (D_Intersect (b, a), D_Intersect (c, d)) when equal_d a c -> D_Intersect (a, D_Union (b, d))
      | (D_Intersect (b, a), D_Intersect (d, c)) when equal_d a c -> D_Intersect (a, D_Union (b, d))
      | (Complement a, a') when equal_d a a' -> D_Atom All
      | (a, Complement a') when equal_d a a' -> D_Atom All
      | x -> D_Union x
    end
    | D_Intersect (left, right) -> begin
      match (opt_d left, opt_d right) with
      | (a, b) when equal_d a b -> a
      | (D_Atom Void, _) | (_, D_Atom Void) -> D_Atom Void
      | (D_Atom All, a) | (a, D_Atom All) -> a
      | (D_Union (a, b), D_Union (c, d)) when equal_d a c -> D_Union (a, D_Intersect (b, d))
      | (D_Union (a, b), D_Union (d, c)) when equal_d a c -> D_Union (a, D_Intersect (b, d))
      | (D_Union (b, a), D_Union (c, d)) when equal_d a c -> D_Union (a, D_Intersect (b, d))
      | (D_Union (b, a), D_Union (d, c)) when equal_d a c -> D_Union (a, D_Intersect (b, d))
      | (Complement a, b) when equal_d a b -> D_Atom Void
      | (a, Complement b) when equal_d a b -> D_Atom Void
      | x -> D_Intersect x
    end
    | Subtract (left, right) -> begin
      match (opt_d left, opt_d right) with
      | (a, b) when equal_d a b -> D_Atom Void
      | (D_Atom Void, _) -> D_Atom Void
      | (D_Atom All, a) -> opt_d (Complement a)
      | (a, D_Atom Void) -> a
      | (_, D_Atom All) -> D_Atom Void
      | (c, D_Intersect(a, b)) ->
        opt_d  (
          if equal_d c a
          then D_Intersect (c, b)
          else D_Union (Subtract (c, a), Subtract(c, b))
        )
      | (c, D_Union(a, b)) -> opt_d (D_Intersect (Subtract (c, a), Subtract(c, b)))
      | (c, Subtract (b, a)) -> opt_d (D_Union (D_Intersect (c, a), (D_Intersect (c, a))))
      | (Complement a, a') when equal_d a a' -> a
      | (a, Complement a') when equal_d a a' -> a
      | x -> Subtract x
    end
    | Complement exp -> begin
      match opt_d exp with
      | D_Intersect (a, b) -> opt_d (D_Intersect (Complement a, Complement b))  (* De Morgan's law (1) *)
      | D_Union (a, b) -> opt_d (D_Union (Complement a, Complement b)) (* De Morgan's law (2) *)
      | Subtract (a, b) -> opt_d (D_Union (Complement a, b))
      | D_Atom Void -> D_Atom All
      | D_Atom All -> D_Atom Void
      | Complement a -> a (* Involution *)
      | x -> Complement x
    end
    | Minimize exp -> begin
      if !minimized then opt_d exp 
      else begin
        minimized := true;
        match opt_d exp with
        | D_Atom Nil -> D_Atom Nil
        | D_Atom Void -> D_Atom Void
        | D_Atom All -> D_Atom All
        | x -> Minimize x
      end
    end
    | Nfa2dfa nfa_exp -> Nfa2dfa (opt_n nfa_exp)
  and opt_n = function
    | N_Atom a -> N_Atom a
    | Nfa nfa -> Nfa nfa
    | Shuffle (left, right) -> begin
      match (opt_n left, opt_n right) with
      | (N_Atom Nil, a) -> a
      | (a, N_Atom Nil) -> a
      | (N_Atom Void, _) -> N_Atom Void
      | (_, N_Atom Void) -> N_Atom Void
      | (N_Atom All, _) -> N_Atom All
      | (_, N_Atom All) -> N_Atom All
      | x -> Shuffle x
    end
    | N_Union (left, right) -> begin
      match (opt_n left, opt_n right) with
      | (a, b) when equal_n a b -> a
      | (N_Atom Void, a) | (a, N_Atom Void) -> a
      | (N_Atom All, _) | (_, N_Atom All) -> N_Atom All
      | (Concat (a, b), Concat (c, d)) when equal_n a c-> Concat (a, N_Union (b, d))
      | (Concat (a, b), Concat (c, d)) when equal_n b d-> Concat (N_Union (a, c), b)
      | x -> N_Union x
    end
    | Concat (left, right) -> begin
      match (opt_n left, opt_n right) with
      | (N_Atom Nil, a) | (a, N_Atom Nil) -> a
      | (N_Atom Void, _) | (_, N_Atom Void) -> N_Atom Void
      | (Star a, Star b) when equal_n a b -> Star a
      | x -> Concat x
    end
    | Star exp -> begin
      match opt_n exp with
      | N_Atom Nil | N_Atom Void -> N_Atom Nil
      | Star a -> Star a
      | x -> Star x
    end
    | Remove_sinks exp -> begin
      if !sinks_removed then opt_n exp
      else begin
        sinks_removed := true;
        match opt_n exp with
        | N_Atom a -> N_Atom a
        | x -> Remove_sinks x
      end
    end
    | Dfa2nfa dfa_exp -> Dfa2nfa (opt_d dfa_exp) in
  let opt_b = function
    | Accepts (exp, word) -> Accepts (opt_d exp, word)
    | Is_empty exp -> Is_empty (opt_d exp)
    | Is_sink (exp, state) -> Is_sink (opt_d exp, state)
    | Equal (left, right) -> Equal (opt_d left, opt_d right)
    | Equivalent (left, right) -> Equivalent (opt_d left, opt_d right)
    | Contains (left, right) -> Contains (opt_d left, opt_d right)
    | N_Accepts (exp, word) -> N_Accepts (opt_n exp, word)
    | N_Is_empty exp -> N_Is_empty (opt_n exp)
    | N_Is_sink (exp, state) -> N_Is_sink (opt_n exp, state)
    | N_Equal (left, right) -> N_Equal (opt_n left, opt_n right) in
  match wrapped_exp with
  | Dfa_exp d -> Dfa_exp (opt_d d)
  | Nfa_exp n -> Nfa_exp (opt_n n)
  | Bool_exp b -> Bool_exp (opt_b b);;

let rec eval alp wrapped_exp = 
  let open Regular in
  let rec eval_d = 
    let open DFA in
    function
    | D_Atom Nil -> make_nil alp
    | D_Atom Void -> make_void alp
    | D_Atom All -> make_all alp
    | Dfa dfa -> flatten dfa
    | D_Union (left, right) -> flatten (union (eval_d left) (eval_d right))
    | D_Intersect (left, right) -> flatten (intersection (eval_d left) (eval_d right))
    | Subtract (left, right) -> flatten (subtract (eval_d left) (eval_d right))
    | Complement exp -> complement (eval_d exp)
    | Minimize exp -> minimize (eval_d exp)
    | Nfa2dfa nfa_exp -> flatten (nfa_to_dfa (eval_n nfa_exp))
  and eval_n = 
    let open NFA in
    function
    | N_Atom Nil -> make_nil alp
    | N_Atom Void -> make_void alp
    | N_Atom All -> make_all alp
    | Nfa nfa -> flatten nfa
    | Shuffle (left, right) -> flatten (shuffle (eval_n left) (eval_n right))
    | N_Union (left, right) -> flatten (union (eval_n left) (eval_n right))
    | Concat (left, right) -> flatten (concat (eval_n left) (eval_n right))
    | Star exp -> flatten (star (eval_n exp))
    | Remove_sinks exp -> remove_sink_states (eval_n exp)
    | Dfa2nfa dfa_exp -> dfa_to_nfa (eval_d dfa_exp) in
  let eval_b = function
    | Accepts (exp, word) -> DFA.accepts (eval_d exp) word
    | Is_empty exp -> DFA.is_empty (eval_d exp)
    | Is_sink (exp, state) -> DFA.is_sink (eval_d exp) state
    | Equal (left, right) -> DFA.equal (eval_d left) (eval_d right)
    | Equivalent (left, right) -> DFA.is_equivalent_to (eval_d left ) (eval_d right)
    | Contains (left, right) -> DFA.contains (eval_d left) (eval_d right)
    | N_Accepts (exp, word) -> NFA.accepts (eval_n exp) word
    | N_Is_empty exp -> NFA.is_empty (eval_n exp)
    | N_Is_sink (exp, state) -> NFA.is_sink (eval_n exp) state
    | N_Equal (left, right) -> NFA.equal (eval_n left) (eval_n right) in
  match wrapped_exp with
  | Dfa_exp d -> R_dfa (eval_d d)
  | Nfa_exp n -> R_nfa (eval_n n)
  | Bool_exp b -> R_bool (eval_b b);;

(* ========================================================================= *)

module DFAexp = struct
  let get_alphabet d = get_alphabet (Dfa_exp d);;

  let equal a b = equal (Dfa_exp a) (Dfa_exp b);;

  let optimize d = 
    match optimize (Dfa_exp d) with
    | Dfa_exp d -> d
    | _ -> failwith "something went wrong with optimize function";;

  let eval alp d = 
    match eval alp (Dfa_exp d) with
    | R_dfa dfa -> dfa
    | _ -> failwith "something went wrong with eval function";;

  let string_of_cons = function
    | D_Atom Nil -> "Nil"
    | D_Atom Void -> "Void"
    | D_Atom All -> "All"
    | Dfa _ -> "Dfa"
    | D_Union _ -> "Union"
    | D_Intersect _ -> "D_Intersect"
    | Subtract _ -> "Subtract"
    | Complement _ -> "Complement"
    | Minimize _ -> "Minimize"
    | Nfa2dfa _ -> "Nfa2dfa";;
end;;

module NFAexp = struct
  let get_alphabet d = get_alphabet (Nfa_exp d);;

  let equal a b = equal (Nfa_exp a) (Nfa_exp b);;

  let optimize d = 
    match optimize (Nfa_exp d) with
    | Nfa_exp d -> d
    | _ -> failwith "something went wrong with optimize function";;

  let eval alp d = 
    match eval alp (Nfa_exp d) with
    | R_nfa dfa -> dfa
    | _ -> failwith "something went wrong with eval function";;

  let string_of_cons = function
    | N_Atom Nil -> "N_Nil"
    | N_Atom Void -> "N_Void"
    | N_Atom All -> "N_All"
    | Nfa _ -> "Nfa"
    | Shuffle _ -> "Shuffle"
    | N_Union _ -> "N_Union"
    | Concat _ -> "Concat"
    | Star _ -> "Star"
    | Remove_sinks _ -> "Remove_sinks"
    | Dfa2nfa _ -> "Dfa2nfa";;  
end;;

module Boolexp = struct
  let get_alphabet d = get_alphabet (Bool_exp d);;

  let equal a b = equal (Bool_exp a) (Bool_exp b);;

  let optimize d = 
    match optimize (Bool_exp d) with
    | Bool_exp d -> d
    | _ -> failwith "something went wrong with optimize function";;

  let eval alp d = 
    match eval alp (Bool_exp d) with
    | R_bool dfa -> dfa
    | _ -> failwith "something went wrong with eval function";;

  let string_of_cons = function
    | Accepts _ -> "Accepts"
    | Is_empty _ -> "Is_empty"
    | Is_sink _ -> "Is_sink"
    | Equal _ -> "Equal"
    | Equivalent _ -> "Equivalent"
    | Contains _ -> "Contains"
    | N_Accepts _ -> "N_Accepts"
    | N_Is_empty _ -> "N_Is_empty"
    | N_Is_sink _ -> "N_Is_sink"
    | N_Equal _ -> "N_Equal";;
end;;