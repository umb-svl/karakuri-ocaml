(* representation of a node -- must be hashable *)
module Node = struct
  type t = int
  let compare = compare
  let hash = Hashtbl.hash
  let equal = (=)
end;;

(* representation of an edge -- must be comparable *)
module Edge = struct
  type t = string
  let compare = compare
  let equal = (=)
  let default = ""
end;;

(* a functional/persistent graph *)
module G = Graph.Persistent.Digraph.ConcreteBidirectionalLabeled(Node)(Edge)

(* module for creating dot-files *)
module Dot = Graph.Graphviz.Dot(struct
  include G (* use the graph module from above *)
  let edge_attributes (a, e, b) = [`Label e; `Color 4711]
  let default_edge_attributes _ = []
  let get_subgraph _ = None
  let vertex_attributes _ = [`Shape `Circle]
  let vertex_name v = string_of_int v
  let default_vertex_attributes _ = []
  let graph_attributes _ = []
end);;

let from_dfa (dfa : (int, string) Regular.DFA.t) filename_out =
  let g = ref G.empty in
  let edges_iter_func (src, char, dst) =
    g := G.add_edge_e !g (G.E.create (G.V.create src) char (G.V.create dst)) in
  Stream.iter edges_iter_func (Regular.DFA.edges dfa);
  let file = open_out_bin (filename_out ^ ".dot") in
  Dot.output_graph file !g;;

let from_nfa (nfa : (int, string) Regular.NFA.t) filename_out =
  let g = ref G.empty in
  let edges_iter_func (src, char_opt, dsts) =
    let char = match char_opt with
    | Some char -> char
    | None -> "ε" in
  List.iter (fun dst -> 
    g := G.add_edge_e !g (G.E.create (G.V.create src) char (G.V.create dst))
  ) dsts in
  Stream.iter edges_iter_func (Regular.NFA.edges nfa);
  let file = open_out_bin (filename_out ^ ".dot") in
  Dot.output_graph file !g;;
