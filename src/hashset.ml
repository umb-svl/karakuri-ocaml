(* wrap Hashtbl as Hashset *)
type 'a t = { tbl: ('a, unit) Hashtbl.t }

let create initial_size =
  { tbl = Hashtbl.create initial_size }

let empty () =
  { tbl = Hashtbl.create 0 }

let clear h =
  Hashtbl.clear h.tbl

let reset h =
  (* Empty and shrink the size of the bucket table to its initial size *)
  Hashtbl.reset h.tbl

let copy h =
  { tbl = Hashtbl.copy h.tbl }

let is_empty h =
  Hashtbl.length h.tbl = 0

let mem h x =
  Hashtbl.mem h.tbl x

let add h x =
  Hashtbl.replace h.tbl x ()

let remove h x =
  Hashtbl.remove h.tbl x

let update h1 h2 =
  (* add elements in h2 to h1 *)
  Hashtbl.iter (fun x () -> Hashtbl.replace h1.tbl x ()) h2.tbl

let union h1 h2 =
  let new_tbl = Hashtbl.copy h1.tbl in
  Hashtbl.iter (fun x () -> Hashtbl.replace new_tbl x ()) h2.tbl;
  { tbl = new_tbl }

let inter h1 h2 =
  let new_tbl = Hashtbl.create (Hashtbl.length h1.tbl) in
  Hashtbl.iter (fun x () -> if Hashtbl.mem h1.tbl x then Hashtbl.replace new_tbl x ()) h2.tbl;
  { tbl = new_tbl }

let disjoint h1 h2 =
  try
    Hashtbl.iter (fun x () -> if (Hashtbl.mem h1.tbl x) then raise_notrace Exit) h2.tbl;
    true
  with Stdlib.Exit -> false

let diff h1 h2 =
  (* elements of h1 that are not in h2. *)
  let new_tbl = Hashtbl.create (Hashtbl.length h1.tbl) in
  Hashtbl.iter (fun x () -> if not (Hashtbl.mem h2.tbl x) then Hashtbl.replace new_tbl x ()) h1.tbl;
  { tbl = new_tbl }

let compare h1 h2 =
  if h1.tbl = h2.tbl then 0 else if h1.tbl > h2.tbl then 1 else -1

let subset h1 h2 =
  (* tests whether the set h1 is a subset of the set h2 *)
  try
    Hashtbl.iter (fun x () -> if not (Hashtbl.mem h2.tbl x) then raise_notrace Exit) h1.tbl;
    true
  with Stdlib.Exit -> false

let equal h1 h2 =
  ((Hashtbl.length h1.tbl) = (Hashtbl.length h2.tbl)) && (subset h1 h2)

let iter f h =
  Hashtbl.iter (fun x () -> f x) h.tbl

let iteri f h =
  let idx = ref 0 in
  Hashtbl.iter (fun x () -> f !idx x; idx := !idx + 1) h.tbl

let map f h =
  let new_tbl = Hashtbl.create (Hashtbl.length h.tbl) in
  Hashtbl.iter (fun x () -> Hashtbl.replace new_tbl (f x) ()) h.tbl;
  { tbl = new_tbl }

let fold f h init =
  Hashtbl.fold (fun x () -> f x) h.tbl init

let for_all p h =
  try
    Hashtbl.iter (fun x () -> if not (p x) then raise_notrace Exit) h.tbl;
    true
  with Stdlib.Exit -> false

let exists p h =
  try
    Hashtbl.iter (fun x () -> if p x then raise_notrace Exit) h.tbl;
    false
  with Stdlib.Exit -> true

let filter p h =
  (* h is unchanged, return a new filtered Hashset *)
  let new_tbl = Hashtbl.create (Hashtbl.length h.tbl) in
  Hashtbl.iter (fun x () -> if p x then Hashtbl.replace new_tbl x ()) h.tbl;
  { tbl = new_tbl }

let partition p h =
  let satisfy_tbl = Hashtbl.create (Hashtbl.length h.tbl) in
  let unsatisfy_tbl = Hashtbl.create (Hashtbl.length h.tbl) in
  Hashtbl.iter (fun x () -> if p x then Hashtbl.replace satisfy_tbl x () else Hashtbl.replace unsatisfy_tbl x ()) h.tbl;
  ({ tbl = satisfy_tbl }, { tbl = unsatisfy_tbl })

let cardinal h =
  Hashtbl.length h.tbl

(* ========================================================================= *)
let of_list ?init_size l =
  let init_size = match init_size with
    | Some int -> int
    | None -> List.length l in
  let tbl = Hashtbl.create init_size in
  List.iter (fun x -> Hashtbl.replace tbl x ()) l; 
  { tbl = tbl }

let of_stream ?(approx_stream_size=1024) s =
  (* do not use with an infinite stream *)
  let tbl = Hashtbl.create approx_stream_size in
  Stream.iter (fun x -> Hashtbl.replace tbl x ()) s;
  { tbl = tbl }

let to_list h =
  (* random order *)
  let list_ptr = ref [] in
  Hashtbl.iter (fun x () -> list_ptr := x :: !list_ptr) h.tbl;
  !list_ptr

let to_stream h =
  (* random order *)
  Stream.of_list (to_list h)