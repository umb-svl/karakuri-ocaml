module NFA = struct
  let of_json json to_st filter_st to_char =
    let open Yojson.Basic.Util in
    let start_state = json |> member "start_state" |> to_st in 
    let accepted_states = json |> member "accepted_states" |> to_list |> filter_st in 
    let js_edges = json |> member "edges" |> to_list in 
    let edges_tbl = Hashtbl.create !Regular.init_size in
    let edges_iter_func js_edge =
      let src = js_edge |> member "src" |> to_st in
      let char_opt = js_edge |> member "char" |> (to_option to_char) in
      let dst = js_edge |> member "dst" |> to_st in
      let src_char = (src, char_opt) in
      match Hashtbl.find_opt edges_tbl src_char with
      | None -> Hashtbl.replace edges_tbl src_char [dst]
      | Some dsts -> Hashtbl.replace edges_tbl src_char (dst :: dsts) in
    List.iter edges_iter_func js_edges;
    Regular.NFA.from_transition_table
      edges_tbl
      start_state
      (Accepted_List accepted_states)

  let to_json (nfa : ('S, 'A) Regular.NFA.t) st_to_js char_to_js  =
    let start_state = ("start_state", st_to_js nfa.start_state) in 
    let accepted_states_ptr = ref [] in
    Stream.iter 
      (fun st -> accepted_states_ptr := (st_to_js st) :: !accepted_states_ptr) 
      (Regular.NFA.end_states nfa);
    let accepted_states = ("accepted_states", `List !accepted_states_ptr) in
    let char_opt_to_js char_opt =
      match char_opt with
      | None -> `Null
      | Some char -> char_to_js char in
    let edges_ptr = ref [] in
    let edges_iter_func (src, char_opt, dsts) =
      let src_jsv = st_to_js src in
      let char_jsv = char_opt_to_js char_opt in
      let dsts_iter_func dst =
        edges_ptr := (
        `Assoc [("src", src_jsv); ("char", char_jsv); ("dst", st_to_js dst)]
        ) :: !edges_ptr in
      List.iter dsts_iter_func dsts in
    Stream.iter edges_iter_func (Regular.NFA.edges nfa);
    let edges = ("edges", `List !edges_ptr) in
    let json = `Assoc [start_state; accepted_states; edges] in
    json

  let to_fsm (nfa : ('S, 'A) Regular.NFA.t) str_of_st str_of_char =
    let start = nfa.start_state in
    let state_to_int = Hashtbl.create !Regular.init_size in
    Hashtbl.add state_to_int start 1;
    let states_list = ref [start] in
    let curr_state = ref 2 in
    (* Build bijection between states and integers *)
    let states_iter_func st =
      match Hashtbl.find_opt state_to_int st with
      | Some _ -> ()
      | None -> begin
        let st_idx = !curr_state in
        curr_state := !curr_state + 1;
        Hashtbl.add state_to_int st st_idx;
        states_list := st :: !states_list;
      end in
    Stream.iter states_iter_func (Regular.NFA.states nfa);
    let cardinal = Hashtbl.length state_to_int in
    assert (cardinal = (List.length !states_list));
    let open Buffer in
    let open Printf in
    let buffer = Buffer.create !Regular.init_size in
    add_string buffer (sprintf "q(%d) State" cardinal);
    List.iter (fun st -> add_string buffer (sprintf " \"%s\"" (str_of_st st))) (List.rev !states_list);
    add_string buffer "\n---";
    for st = 0 to (cardinal - 1) do
      add_string buffer (sprintf "\n%d" st)
    done;
    add_string buffer "\n---";
    let edges_iter_func (src, char_opt, dsts) =
      let src = Hashtbl.find state_to_int src in
      let char_str = match char_opt with
        | Some char -> str_of_char char
        | None -> "ε" in
      let dsts_iter_func dst =
        let dst = Hashtbl.find state_to_int dst in
        add_string buffer (sprintf "\n%d %d \"%s\"" src dst char_str) in
      List.iter dsts_iter_func dsts in
    Stream.iter edges_iter_func (Regular.NFA.edges nfa);
    contents buffer

end

module DFA = struct
  let of_json json to_st filter_st to_char =
    let open Yojson.Basic.Util in
    let start_state = json |> member "start_state" |> to_st in 
    let accepted_states = json |> member "accepted_states" |> to_list |> filter_st in 
    let js_edges = json |> member "edges" |> to_list in 
    let edges_tbl = Hashtbl.create !Regular.init_size in
    let alpha_set = Hashset.create !Regular.init_size in
    let edges_iter_func js_edge =
      let src = js_edge |> member "src" |> to_st in
      let char = js_edge |> member "char" |> to_char in
      let dst = js_edge |> member "dst" |> to_st in
      Hashtbl.replace edges_tbl (src, char) dst;
      Hashset.add alpha_set char in 
    List.iter edges_iter_func js_edges;
    Regular.DFA.create 
      alpha_set
      (Regular.DFA.transition_table_of_hashtbl edges_tbl)
      start_state
      (Accepted_List accepted_states)

  let to_json (dfa : ('S, 'A) Regular.DFA.t) st_to_js char_to_js =
    let start_state = ("start_state", st_to_js dfa.start_state) in 
    let accepted_states_ptr = ref [] in
    Stream.iter 
      (fun st -> accepted_states_ptr := (st_to_js st) :: !accepted_states_ptr) 
      (Regular.DFA.end_states dfa);
    let accepted_states = ("accepted_states", `List !accepted_states_ptr) in
    let edges_ptr = ref [] in
    Stream.iter 
      (fun (src, char, dst) -> edges_ptr := (
        `Assoc [("src", st_to_js src); ("char", char_to_js char); ("dst", st_to_js dst)]
        ) :: !edges_ptr)
      (Regular.DFA.edges dfa);
    let edges = ("edges", `List !edges_ptr) in
    let json = `Assoc [start_state; accepted_states; edges] in
    json

  let to_fsm (dfa : ('S, 'A) Regular.DFA.t) str_of_st str_of_char =
    let start = dfa.start_state in
    let state_to_int = Hashtbl.create !Regular.init_size in
    Hashtbl.add state_to_int start 1;
    let states_list = ref [start] in
    let curr_state = ref 2 in
    (* Build bijection between states and integers *)
    let states_iter_func st =
      match Hashtbl.find_opt state_to_int st with
      | Some _ -> ()
      | None -> begin
        let st_idx = !curr_state in
        curr_state := !curr_state + 1;
        Hashtbl.add state_to_int st st_idx;
        states_list := st :: !states_list;
      end in
    Stream.iter states_iter_func (Regular.DFA.states dfa);
    let cardinal = Hashtbl.length state_to_int in
    assert (cardinal = (List.length !states_list));
    let open Buffer in
    let open Printf in
    let buffer = Buffer.create !Regular.init_size in
    add_string buffer (sprintf "q(%d) State" cardinal);
    List.iter (fun st -> add_string buffer (sprintf " \"%s\"" (str_of_st st))) (List.rev !states_list);
    add_string buffer "\n---";
    for st = 0 to (cardinal - 1) do
      add_string buffer (sprintf "\n%d" st)
    done;
    add_string buffer "\n---";
    let edges_iter_func (src, char, dst) =
      let src = Hashtbl.find state_to_int src in
      let dst = Hashtbl.find state_to_int dst in
      add_string buffer (sprintf "\n%d %d \"%s\"" src dst (str_of_char char)) in
    Stream.iter edges_iter_func (Regular.DFA.edges dfa);
    contents buffer
end

module LTL = struct
  let rec parse_ltl j : Ltl.ltl =
    let open Ltl in
    let open Yojson.Basic.Util in
    match j with
    | `Bool b -> Ltl.Bool b
    | `String x -> Ltl.Var x
    | `Assoc [("implies", `List [l;r])] -> Or(Neg(parse_ltl l), parse_ltl r)
    | `Assoc [("eventually", j)] -> eventually (parse_ltl j)
    | `Assoc [("always", j)] -> always (parse_ltl j)
    | `Assoc [("not", j)] -> Neg (parse_ltl j)
    | `Assoc [("and", `List [l; r] )] -> And (parse_ltl l, parse_ltl r)
    | `Assoc [("or", `List [l; r])] -> Or (parse_ltl l, parse_ltl r)
    | `Assoc [("next", j)] -> Next (parse_ltl j)
    | `Assoc [("until", `List [l;r])] -> Until(parse_ltl l, parse_ltl r)
    | _ -> failwith ("parsing error: " ^ Yojson.Basic.to_string j)

  let parse j : string list * Ltl.ltl =
    let open Yojson.Basic.Util in
    try
      let vars : string list = j |> member "vars" |> convert_each to_string in
      let exp = j |> member "exp" in
      (vars, parse_ltl exp)
    with
      | Yojson.Basic.Util.Type_error _ ->
        failwith ("Error parsing: " ^ (Yojson.Basic.to_string j))

  let of_json j : (int, string) Regular.NFA.t =
    let (vars, p) = parse j in
    Ltl2nfa.make vars p
    |> Regular.NFA.flatten

end

(* ========================================================================= *)
(* Expression *)

type js_wrap =
  | Js_dfa_exp of Yojson.Basic.t
  | Js_nfa_exp of Yojson.Basic.t
  | Js_bool_exp of Yojson.Basic.t

(* ========================================================================= *)
(* private functions - not meant for users to use *)

let exp_of_json wrapped_json to_st filter_st to_char filter_char =
  let open Exp in
  let open Yojson.Basic.Util in
  let minimized = ref false in
  let rec to_exp_d js_assoc =
    let (str, json) = List.hd (to_assoc js_assoc) in
    match str with
    | "nil" -> D_Atom Nil
    | "void" -> D_Atom Void
    | "all" -> D_Atom All
    | "dfa" -> begin
      let json = match json with
        | `String k -> Yojson.Basic.from_file k
        | _ -> json
      in
      if !minimized then
        (* optimize minimization by removing sinks *)
        let nfa = NFA.of_json json to_st filter_st to_char in
        let nfa_sinkless = Regular.NFA.remove_sink_states nfa in
        let dfa = Regular.nfa_to_dfa nfa_sinkless in
        let dfa_flat = Regular.DFA.flatten dfa in
        Dfa dfa_flat
      else
        let dfa = DFA.of_json json to_st filter_st to_char in
        let dfa_flat = Regular.DFA.flatten dfa in
        Dfa dfa_flat
    end
    | "union" ->
      let left = json |> member "left" in
      let right = json |> member "right" in
      D_Union (to_exp_d left, to_exp_d right)
    | "intersection" ->
      let left = json |> member "left" in
      let right = json |> member "right" in
      D_Intersect (to_exp_d left, to_exp_d right)
    | "subtract" ->
      let left = json |> member "left" in
      let right = json |> member "right" in
      Subtract (to_exp_d left, to_exp_d right)
    | "complement" -> Complement (to_exp_d json)
    | "minimize" -> begin
      minimized := true;
      Minimize (to_exp_d json)
    end
    | "nfa2dfa" -> Nfa2dfa (to_exp_n json)
    | _ -> failwith ("mismatching constructor for DFA expression: " ^ str)
  and to_exp_n js_assoc =
    let (str, json) = List.hd (to_assoc js_assoc) in
    match str with
    | "n_nil" -> N_Atom Nil
    | "n_void" -> N_Atom Void
    | "n_all" -> N_Atom All
    | "ltl" -> Nfa (LTL.of_json json)
    | "nfa" ->
      let json = match json with
        | `String k -> Yojson.Basic.from_file k
        | _ -> json
      in
      let nfa = NFA.of_json json to_st filter_st to_char in
      let nfa_flat = Regular.NFA.flatten nfa in
      Nfa nfa_flat
    | "shuffle" ->
      let left = json |> member "left" in
      let right = json |> member "right" in
      Shuffle (to_exp_n left, to_exp_n right)
    | "n_union" ->
      let left = json |> member "left" in
      let right = json |> member "right" in
      N_Union (to_exp_n left, to_exp_n right)
    | "concat" ->
      let left = json |> member "left" in
      let right = json |> member "right" in
      Concat (to_exp_n left, to_exp_n right)
    | "star" -> Star (to_exp_n json)
    | "remove_sinks" -> Remove_sinks (to_exp_n json)
    | "dfa2nfa" -> Dfa2nfa (to_exp_d json)
    | _ -> failwith ("mismatching constructor for NFA expression: " ^ str) in
  let to_exp_b js_assoc =
    let (str, json) = List.hd (to_assoc js_assoc) in
    match str with
    | "accepts" ->
      let exp = json |> member "exp" in
      let word = json |> member "word" |> to_list |> filter_char in
      Accepts (to_exp_d exp, word)
    | "is_empty" -> Is_empty (to_exp_d json)
    | "is_sink" ->
      let exp = json |> member "exp" in
      let state = json |> member "state" |> to_int in
      Is_sink (to_exp_d exp, state)
    | "equal" -> 
      let left = json |> member "left" in
      let right = json |> member "right" in
      Equal (to_exp_d left, to_exp_d right)
    | "equivalent" ->
      let left = json |> member "left" in
      let right = json |> member "right" in
      Equivalent (to_exp_d left, to_exp_d right)
    | "contains" ->
      let left = json |> member "left" in
      let right = json |> member "right" in
      Contains (to_exp_d left, to_exp_d right)
    (* NFA bool *)
    | "n_accepts" ->
      let exp = json |> member "exp" in
      let word = json |> member "word" |> to_list |> filter_char in
      N_Accepts (to_exp_n exp, word)
    | "n_is_empty" -> N_Is_empty (to_exp_n json)
    | "n_is_sink" ->
      let exp = json |> member "exp" in
      let state = json |> member "state" |> to_int in
      N_Is_sink (to_exp_n exp, state)
    | "n_equal" -> 
      let left = json |> member "left" in
      let right = json |> member "right" in
      N_Equal (to_exp_n left, to_exp_n right)
    | _ -> failwith ("mismatching constructor for Boolean expression: " ^ str) in
  match wrapped_json with
  | Js_dfa_exp jsd -> Dfa_exp (to_exp_d jsd)
  | Js_nfa_exp jsn -> Nfa_exp (to_exp_n jsn)
  | Js_bool_exp jsb -> Bool_exp (to_exp_b jsb)

let exp_to_json wrapped_exp st_to_js char_to_js =
  let open Exp in
  let open Yojson.Basic.Util in
  let rec to_js_d = function
    | D_Atom Nil -> `Assoc [("nil", `Null)]
    | D_Atom Void -> `Assoc [("void", `Null)]
    | D_Atom All -> `Assoc [("all", `Null)]
    | Dfa dfa -> `Assoc [("dfa", DFA.to_json dfa st_to_js char_to_js)]
    | D_Union (left, right) -> 
      let pair = `Assoc [("left", to_js_d left); ("right", to_js_d right)] in
      `Assoc [("union", pair)]
    | D_Intersect (left, right) -> 
      let pair = `Assoc [("left", to_js_d left); ("right", to_js_d right)] in
      `Assoc [("intersection", pair)]
    | Subtract (left, right) -> 
      let pair = `Assoc [("left", to_js_d left); ("right", to_js_d right)] in
      `Assoc [("subtract", pair)]
    | Complement x -> `Assoc [("complement", to_js_d x)]
    | Minimize x -> `Assoc [("minimize", to_js_d x)] 
    | Nfa2dfa x -> `Assoc [("nfa2dfa", to_js_n x)] 
  and to_js_n = function
    | N_Atom Nil -> `Assoc [("n_nil", `Null)]
    | N_Atom Void -> `Assoc [("n_void", `Null)]
    | N_Atom All -> `Assoc [("n_all", `Null)]
    | Nfa nfa -> `Assoc [("nfa", NFA.to_json nfa st_to_js char_to_js)]
    | Shuffle (left, right) ->
      let pair = `Assoc [("left", to_js_n left); ("right", to_js_n right)] in
      `Assoc [("shuffle", pair)]
    | N_Union (left, right) ->
      let pair = `Assoc [("left", to_js_n left); ("right", to_js_n right)] in
      `Assoc [("n_union", pair)]
    | Concat (left, right) ->
      let pair = `Assoc [("left", to_js_n left); ("right", to_js_n right)] in
      `Assoc [("concat", pair)]
    | Star x -> `Assoc [("star", to_js_n x)]
    | Remove_sinks x -> `Assoc [("remove_sinks", to_js_n x)]
    | Dfa2nfa x -> `Assoc [("dfa2nfa", to_js_d x)] in
  let to_js_b = function
    | Accepts (exp, word) -> 
      let pair = `Assoc [("exp", to_js_d exp); ("word", `List (List.map char_to_js word))] in
      `Assoc [("accepts", pair)]
    | Is_empty exp -> `Assoc [("is_empty", to_js_d exp)]
    | Is_sink (exp, state) ->
      let pair = `Assoc [("exp", to_js_d exp); ("state", `Int state)] in
      `Assoc [("is_sink", pair)]
    | Equal (left, right) -> 
      let pair = `Assoc [("left", to_js_d left); ("right", to_js_d right)] in
      `Assoc [("equal", pair)]
    | Equivalent (left, right) ->
      let pair = `Assoc [("left", to_js_d left); ("right", to_js_d right)] in
      `Assoc [("equivalent", pair)]
    | Contains (left, right) ->
      let pair = `Assoc [("left", to_js_d left); ("right", to_js_d right)] in
      `Assoc [("contains", pair)]
    | N_Accepts (exp, word) ->
      let pair = `Assoc [("exp", to_js_n exp); ("word", `List (List.map char_to_js word))] in
      `Assoc [("n_accepts", pair)]
    | N_Is_empty exp -> `Assoc [("n_is_empty", to_js_n exp)]
    | N_Is_sink (exp, state) ->
      let pair = `Assoc [("exp", to_js_n exp); ("state", `Int state)] in
      `Assoc [("n_is_sink", pair)]
    | N_Equal (left, right) ->
      let pair = `Assoc [("left", to_js_n left); ("right", to_js_n right)] in
      `Assoc [("n_equal", pair)] in
  match wrapped_exp with
  | Dfa_exp d -> to_js_d d
  | Nfa_exp n -> to_js_n n
  | Bool_exp b -> to_js_b b
(* ========================================================================= *)

module DFAexp = struct
  let of_json json to_st filter_st to_char filter_char =
    match exp_of_json (Js_dfa_exp json) to_st filter_st to_char filter_char with
    | Dfa_exp d -> d
    | _ -> failwith "something went wrong with exp_of_json function"

  let to_json exp st_to_js char_to_js =
    exp_to_json (Dfa_exp exp) st_to_js char_to_js
end

module NFAexp = struct
  let of_json json to_st filter_st to_char filter_char =
    match exp_of_json (Js_nfa_exp json) to_st filter_st to_char filter_char with
    | Nfa_exp d -> d
    | _ -> failwith "something went wrong with exp_of_json function"

  let to_json exp st_to_js char_to_js =
    exp_to_json (Nfa_exp exp) st_to_js char_to_js
end

module Boolexp = struct
  let of_json json to_st filter_st to_char filter_char =
    match exp_of_json (Js_bool_exp json) to_st filter_st to_char filter_char with
    | Bool_exp d -> d
    | _ -> failwith "something went wrong with exp_of_json function"

  let to_json exp st_to_js char_to_js =
    exp_to_json (Bool_exp exp) st_to_js char_to_js
end
