open Regular

let make (variables:string list) (l:Ltl.ltl) : (Ltl.ltl_e, string) NFA.t =
  let alphabet = Hashset.of_list variables in
  NFA.create alphabet (fun p w ->
    match w with
    | None -> []
    | Some w -> Ltl.transition p [w]
  ) (Ltl.ltl_to_e l) (Accepted_Function Ltl.is_final)
